<?php

namespace App\ArgumentResolver;

use App\Dto\RequestDtoInterface;
use App\Transform\RequestDtoTransform;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

final class RequestDtoValueResolver implements ArgumentValueResolverInterface
{
    /**
     * @var RequestDtoTransform
     */
    private RequestDtoTransform $transformer;

    /**
     * @param RequestDtoTransform $transformer
     */
    public function __construct(RequestDtoTransform $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return is_a($argument->getType(), RequestDtoInterface::class, true);
    }

    /**
     * @param Request $request
     * @return string
     */
    private function getSerializeFormat(Request $request): string
    {
        static $supportFormats = ['json', 'xml'];
        static $defaultFormat = 'json';

        $format = $request->getContentType() ?? $defaultFormat;

        if (!\in_array($format, $supportFormats, true)) {
            $format = $defaultFormat;
        }

        return $format;
    }

    /**
     * @return iterable
     * @throws ExceptionInterface
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $format = $this->getSerializeFormat($request);

        try {
            $obj = $this->transformer->transform($request, $argument->getType(), $format);
        } catch (\TypeError | NotEncodableValueException $e) {
            $problemMimeType = 'application/problem+' . $format;

            throw new HttpException(
                400,
                'Bad Request',
                $e,
                ['Content-Type' => $problemMimeType]
            );
        }

        yield $obj;
    }
}
