<?php

namespace App\Http;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse extends JsonResponse
{
    public function __construct(
        mixed $data = null,
        ?string $message = null,
        int $status = 200,
        array $headers = [],
        bool $json = false
    ) {
        $data = [
            'status' => $status,
            'msg' => $message,
            'data' => $data
        ];

        $this->encodingOptions = $this->getEncodingOptions() | JSON_INVALID_UTF8_IGNORE;
        parent::__construct($data, $status, $headers, $json);
    }
}
