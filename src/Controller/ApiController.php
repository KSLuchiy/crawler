<?php

namespace App\Controller;

use App\Dto\Request\ComplaintBody;
use App\Dto\Request\FasItemFilter;
use App\Http\ApiResponse;
use App\Repository\ComplaintRepository;
use App\Repository\ComplaintResultRepository;
use App\Repository\ComplaintStatusRepository;
use App\Repository\ControlOrganizationRepository;
use App\Service\ComplaintService;
use App\Service\DocumentService;
use App\Transform\ResponseDtoTransform;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class ApiController extends AbstractController
{
    public function __construct(private ResponseDtoTransform $transform)
    {
    }

    #[Route('/get-control-body', name: 'api.get-control-body')]
    public function getControlBody(ControlOrganizationRepository $repository): Response {
        try {
            $response = $this->transform->transformArray(
                $repository->findAllControlOrganization(),
                'App\Dto\Response\ControlBody[]'
            );
        } catch (Throwable $e) {
            return new ApiResponse($e->getTraceAsString(), $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new ApiResponse($response);
    }

    #[Route('/get-fas-type', name: 'api.get-fas-type')]
    public function getFasType(ComplaintStatusRepository $repository): Response {
        try {
            $response = $this->transform->transformArray(
                $repository->findAllComplaintStatus(),
                'App\Dto\Response\FasType[]'
            );
        } catch (Throwable $e) {
            return new ApiResponse($e->getTraceAsString(), $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new ApiResponse($response);
    }


    #[Route('/fas-items', name: 'api.fas-items')]
    public function getFasItems(ComplaintService $complaintService, FasItemFilter $fasItem): Response {
        try {
            $complaintsInfo = $complaintService->findByQuery($fasItem);
        } catch (Throwable $e) {
            return new ApiResponse($e->getTraceAsString(), $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new ApiResponse([
            'entities' => $complaintsInfo
        ]);
    }

    #[Route('/fas-items/{id}', name: 'api.fas-item.id')]
    public function getFasItemsById(int $id, ComplaintService $complaintService): Response {
        try {
            $complaintsInfo = $complaintService->findById($id);
        } catch (Throwable $e) {
            return new ApiResponse($e->getTraceAsString(), $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new ApiResponse($complaintsInfo);
    }

    #[Route('/page/complaint/{id}/{keywords}/{searchPhrase}', name: 'api.complaint-body', defaults: ['keywords' => null, 'searchPhrase' => false])]
    public function getComplaintBody(
        ComplaintRepository $complaintRepository,
        ComplaintService $complaintService,
        DocumentService $documentService,
        ComplaintBody $complaintBody
    ): Response {
        try {
            $complaint = $complaintRepository->find($complaintBody->getId());
            if (!$complaint) {
                throw new Exception('Complaint not found.', Response::HTTP_NOT_FOUND);
            }

            $response = $complaintService->getComplaintBody($complaint, $complaintBody);
            if (empty($response)) {
                $documentService->tryFindComplaintBody($complaint);
                $response = $complaintService->getComplaintBody($complaint, $complaintBody);
            }

            if (empty($response)) {
                throw new Exception('Complaint document not found.', Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
            }
        } catch (Throwable $e) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
        }

        return new Response($response);
    }

    #[Route('/count-complaint', name: 'api.count-complaint')]
    public function getCountComplaint(
        ComplaintRepository $complaintRepository,
        ComplaintResultRepository $complaintResultRepository
    ): Response {
        try {
            $countComplaint = $complaintRepository->getCountComplaint();
            $dateComplaint = $complaintRepository->getDateComplaint();

            $countComplaintResult = $complaintResultRepository->getCountComplaintResult();
            $dateComplaintResult = $complaintResultRepository->getDateComplaintResult();
        } catch (Throwable $e) {
            return new ApiResponse($e->getTraceAsString(), $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new ApiResponse([
            'complaint' => $countComplaint,
            'complaint_date' => $dateComplaint,
            'complaint_result' => $countComplaintResult,
            'complaint_result_date' => $dateComplaintResult
        ]);
    }
}
