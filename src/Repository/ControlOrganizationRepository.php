<?php

namespace App\Repository;

use App\Entity\ControlOrganization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ControlOrganization|null find($id, $lockMode = null, $lockVersion = null)
 * @method ControlOrganization|null findOneBy(array $criteria, array $orderBy = null)
 * @method ControlOrganization[] findAll()
 * @method ControlOrganization[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ControlOrganizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ControlOrganization::class);
    }

    public function findAllControlOrganization(): array
    {
        $organizations = $this->findBy([
            'archive' => false,
        ], [
            'fullName' => 'ASC',
        ]);
        return (new ArrayCollection($organizations))
            ->map(static function (ControlOrganization $organization) {
                return [
                    'title' => (string) $organization->getFullName(),
                    'value' => $organization->getId()
                ];
            })
            ->toArray();
    }
}
