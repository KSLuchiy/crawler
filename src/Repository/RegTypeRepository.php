<?php

namespace App\Repository;

use App\Entity\RegType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RegType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RegType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RegType[] findAll()
 * @method RegType[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RegTypeRepository extends ServiceEntityRepository
{
    /**
     * RegTypeRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegType::class);
    }
}
