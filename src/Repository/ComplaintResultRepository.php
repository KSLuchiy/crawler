<?php

namespace App\Repository;

use App\Entity\ComplaintResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ComplaintResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComplaintResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComplaintResult[]    findAll()
 * @method ComplaintResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplaintResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComplaintResult::class);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getCountComplaintResult(): int
    {
        $qb = $this->createQueryBuilder('cr');

        return (int) $qb
            ->select('coalesce(count(cr.id), 0)')
            ->join('cr.complaint', 'c')
            ->join('c.controlOrganization', 'o')
            ->where(
                $qb->expr()->eq('o.archive', ':archive')
            )
            ->setParameter('archive', false)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getDateComplaintResult(): ?string
    {
        $qb = $this->createQueryBuilder('cr');

        return $qb
            ->select('coalesce(cr.createdAt, current_timestamp())')
            ->join('cr.complaint', 'c')
            ->join('c.controlOrganization', 'o')
            ->where(
                $qb->expr()->eq('o.archive', ':archive')
            )
            ->setParameter('archive', false)
            ->orderBy('cr.createdAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
