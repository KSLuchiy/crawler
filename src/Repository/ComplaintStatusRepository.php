<?php

namespace App\Repository;

use App\Entity\ComplaintStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ComplaintStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComplaintStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComplaintStatus[] findAll()
 * @method ComplaintStatus[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplaintStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComplaintStatus::class);
    }

    public function findAllComplaintStatus(): array
    {
        return (new ArrayCollection($this->findAll()))
            ->map(static function (ComplaintStatus $status) {
                return [
                    'title' => $status->getName(),
                    'value' => $status->getCode()
                ];
            })
            ->toArray();
    }

    public function findAllComplaintStatusCode(): array
    {
        $data = [];
        foreach ($this->findAll() as $item) {
            $data[$item->getId()] = $item->getCode();
        }

        return $data;
    }
}
