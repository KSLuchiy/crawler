<?php

namespace App\Repository;

use App\Entity\Complaint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Complaint|null find($id, $lockMode = null, $lockVersion = null)
 * @method Complaint|null findOneBy(array $criteria, array $orderBy = null)
 * @method Complaint[]    findAll()
 * @method Complaint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplaintRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Complaint::class);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getCountComplaint(): int
    {
        $qb = $this->createQueryBuilder('c');

        return (int) $qb
            ->select('coalesce(count(c.id), 0)')
            ->join('c.controlOrganization', 'o')
            ->where(
                $qb->expr()->eq('o.archive', ':archive')
            )
            ->setParameter('archive', false)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getDateComplaint(): ?string
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->select('coalesce(c.createdAt, current_timestamp())')
            ->join('c.controlOrganization', 'o')
            ->where(
                $qb->expr()->eq('o.archive', ':archive')
            )
            ->setParameter('archive', false)
            ->orderBy('c.createdAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getComplaintByRegNumberOrComplaintNumber(?string $regNumber, ?string $complaintNumber): ?Complaint
    {
        $qb = $this->createQueryBuilder('c')
            ->setMaxResults(1);

        if (null !== $regNumber) {
            $qb
                ->where('c.regNumber = :regNumber')
                ->setParameter('regNumber', $regNumber);
        } elseif (null !== $complaintNumber) {
            $qb
                ->where('c.complaintNumber = :complaintNumber')
                ->setParameter('complaintNumber', $complaintNumber);
        }

        return $qb
            ->getQuery()
            ->getOneOrNullResult();
    }
}
