<?php

namespace App\Repository;

use App\Entity\IntegrationPacket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IntegrationPacket|null find($id, $lockMode = null, $lockVersion = null)
 * @method IntegrationPacket|null findOneBy(array $criteria, array $orderBy = null)
 * @method IntegrationPacket[] findAll()
 * @method IntegrationPacket[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IntegrationPacketRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IntegrationPacket::class);
    }

    /**
     * @param string $hash
     * @param string $discriminator
     * @return IntegrationPacket|null
     * @throws NonUniqueResultException
     */
    public function findOneByHash(string $hash, string $discriminator): ?IntegrationPacket
    {
        return $this->createQueryBuilder('i')
            ->where('i.hash = :hash AND i INSTANCE OF :discriminator')
            ->setParameters(['hash' => $hash, 'discriminator' => $discriminator])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $name
     * @param string $discriminator
     * @return IntegrationPacket|null
     * @throws NonUniqueResultException
     */
    public function findOneByName(string $name, string $discriminator): ?IntegrationPacket
    {
        return $this->createQueryBuilder('i')
            ->where('i.name = :name AND i INSTANCE OF :discriminator')
            ->setMaxResults(1)
            ->setParameters([
                'name' => $name,
                'discriminator' => $discriminator
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param array $hashes
     * @param string $discriminator
     * @return IntegrationPacket[]
     */
    public function findAllByHash(array $hashes, string $discriminator): array
    {
        return $this->createQueryBuilder('i')
            ->where('i.hash IN (:hashes) AND i INSTANCE OF :discriminator')
            ->setParameters(['hashes' => $hashes, 'discriminator' => $discriminator])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $statuses
     * @param string $discriminator
     * @return IntegrationPacket[]
     */
    public function findAllByStatuses(array $statuses, string $discriminator): array
    {
        return $this->createQueryBuilder('i')
            ->where('i.status IN (:statuses) AND i INSTANCE OF :discriminator')
            ->setParameters(['statuses' => $statuses, 'discriminator' => $discriminator])
            ->getQuery()
            ->getResult();
    }

    private function createQuery(string $status, string $discriminator, array $ignoreIds): QueryBuilder
    {
        $qb = $this->createQueryBuilder('i')
            ->where('i.status = :status AND i INSTANCE OF :discriminator')
            ->setParameters([
                'status' => $status,
                'discriminator' => $discriminator
            ]);

        if (!empty($ignoreIds)) {
            $qb
                ->andWhere($qb->expr()->notIn('i.id', ':ids'))
                ->setParameter('ids', $ignoreIds);
        }

        return $qb;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneByStatus(string $status, string $discriminator, array $ignoreIds): ?IntegrationPacket
    {
        return $this->createQuery($status, $discriminator, $ignoreIds)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneByStatusAndPriority(
        string $status,
        string $discriminator,
        array $ignoreIds
    ): ?IntegrationPacket {
        return $this->createQuery($status, $discriminator, $ignoreIds)
            ->orderBy('i.priority', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
