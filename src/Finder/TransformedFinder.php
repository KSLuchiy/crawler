<?php

namespace App\Finder;

use App\Paginator\RawPaginatorAdapter;
use Elastica\Query;
use FOS\ElasticaBundle\Finder\TransformedFinder as BaseTransformedFinder;

class TransformedFinder extends BaseTransformedFinder
{
    public function createRawPaginatorAdapter($query, array $options = [])
    {
        $query = Query::create($query);

        return new RawPaginatorAdapter($this->searchable, $query, $options);
    }
}
