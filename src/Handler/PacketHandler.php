<?php

namespace App\Handler;

use App\Entity\IntegrationPacket\PacketXml;
use App\Exception\PacketValidationException;
use App\Handler\Packet\HandlerInterface;
use App\Service\FileService;
use App\Service\XmlServiceInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Throwable;

class PacketHandler implements PacketHandlerInterface
{
    public function __construct(
        #[TaggedIterator(HandlerInterface::class)]
        private iterable $handlers,
        private XmlServiceInterface $xmlService,
        private FileService $fileService,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @inheritdoc
     */
    public function handle(PacketXml $packet): bool
    {
        $fileInfo = $this->fileService->getFileInfo($packet->getPath());
        [$version, $type] = $this->xmlService->getPacketInfo($fileInfo->getContents());

        $packet->setVersion($version);

        try {
            $data = $this->xmlService->xmlToArray($fileInfo->getContents());
        } catch (Throwable $e) {
            throw new PacketValidationException($e->getMessage(), $e->getCode(), $e);
        }

        if (empty($data) || $type === null) {
            throw new Exception(sprintf('Not found handler for packet "%i"', $packet->getId()));
        }

        foreach ($this->handlers as $handler) {
            if (!$handler->supports($type)) {
                continue;
            }

            return $this->tryProcessPackage($handler, $packet, $data);
        }

        throw new Exception(sprintf('Not found handler for type "%s" and version "%s"', $type, $version));
    }

    /**
     * Попытка обработки пакета стратегией.
     *
     * @throws Throwable
     */
    private function tryProcessPackage(HandlerInterface $handler, PacketXml $packet, array $data): bool
    {
        try {
            $handler->process($packet, $data);
        } catch (Throwable $e) {
            echo 'ERROR - ' . (new \DateTime())->format('Y-m-d H:i:s') . ' - ' . $e->getMessage() . PHP_EOL;
            $this->logger->info((new \DateTime())->format('Y-m-d H:i:s') . ' - ' . $e->getMessage());

            throw $e;
        }

        return true;
    }
}
