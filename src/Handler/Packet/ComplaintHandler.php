<?php

namespace App\Handler\Packet;

use App\Entity\Complaint;
use App\Entity\Complaint as ComplaintEntity;
use App\Entity\ControlOrganization;
use App\Entity\Direction;
use App\Entity\Document;
use App\Entity\IntegrationPacket;
use App\Entity\Organization;
use App\Entity\Organization\IndividualBusinessman;
use App\Entity\Organization\IndividualPerson;
use App\Entity\Organization\LegalEntity;
use App\Entity\Purchase;
use App\Entity\RegType;
use App\Exception\PacketHandlerException;
use App\Repository\ControlOrganizationRepository;
use App\Repository\DirectionRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PurchaseRepository;
use App\Repository\RegTypeRepository;
use App\Service\ParserDocumentService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Throwable;

class ComplaintHandler extends AbstractHandler
{
    /** @var string */
    private const FZ44 = '44fz';
    protected string $packet = 'complaint';

    public function __construct(
        EntityManagerInterface $entityManager,
        private PurchaseRepository $purchaseRepository,
        private ControlOrganizationRepository $controlOrganizationRepository,
        private OrganizationRepository $organizationRepository,
        private DirectionRepository $directionRepository,
        private RegTypeRepository $regTypeRepository,
        private ParserDocumentService $parserDocumentService
    ) {
        parent::__construct($entityManager);
    }

    /**
     * @throws Throwable
     */
    public function process(IntegrationPacket $packet, array $data): void
    {
        $complaintModel = $this->arrayToObject($data['export']['complaint']);

        $regNumber = $complaintModel->commonInfo->regNumber ?? $this->getRegNumberFromFileName($packet->getName());
        if (empty($regNumber) && empty($complaintModel->commonInfo->complaintNumber)) {
            throw new Exception('Complaint number not found.');
        }

        $this->entityManager->beginTransaction();
        try {
            $complaintEntity = (new ComplaintEntity())
                ->setCreatedAt($this->getCreateDate($complaintModel->commonInfo))
                ->setUpdatedAt($this->getUpdatedDate($complaintModel->commonInfo))
                ->setRegNumber($regNumber)
                ->setComplaintNumber($complaintModel->commonInfo->complaintNumber ?? null)
                ->setVersion($complaintModel->commonInfo->versionNumber ?? Complaint::DEFAULT_VERSION)
                ->setRegType($this->getRegType($complaintModel->commonInfo->regType))
                ->setPurchase($this->getPurchase($complaintModel->object->purchase))
                ->setControlOrganization($this->getControlOrganization($complaintModel->commonInfo))
                ->setApplicantOrganization($this->getApplicantOrganization($complaintModel->applicantNew))
                ->setDirection($this->getDirection())
                ->setExternalId($this->getExternalId($complaintModel->printForm));

            $this->addAttachments($complaintEntity, $complaintModel->attachments ?? null);

            $this->entityManager->persist($complaintEntity);
            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (Throwable $e) {
            $this->entityManager->rollback();
            throw new PacketHandlerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function getExternalId(object $printForm): int
    {
        $parts = parse_url($printForm->url);
        parse_str($parts['query'], $query);
        return $query['id'];
    }

    private function addAttachments(ComplaintEntity $complaint, ?object $attachmentsList)
    {
        if (empty($attachmentsList) || empty($attachmentsList->attachment)) {
            return;
        }

        $attachments = is_array($attachmentsList->attachment) ? $attachmentsList->attachment : [$attachmentsList->attachment];
        foreach ($attachments as $attachment) {
            if (!$this->parserDocumentService->getParser($this->getFileExtension($attachment->fileName))) {
                continue;
            }

            $document = (new Document\ComplaintDocument())
                ->setComplaint($complaint)
                ->setName($attachment->docDescription)
                ->setRealName($attachment->fileName)
                ->setPublishedContentId($attachment->publishedContentId)
                ->setUrl($attachment->url)
                ->setStatus(Document::STATUS_WAIT_CONFIRM);

            $complaint->addDocument($document);
        }
    }

    private function getRegType(string $type): RegType
    {
        return $this->regTypeRepository->findOneBy([
            'code' => $type
        ]);
    }

    /**
     * @throws Exception
     */
    private function getUpdatedDate(object $info): DateTime
    {
        return !empty($info->printFormInfo->editDate) ? new DateTime($info->printFormInfo->editDate) : new DateTime();
    }

    /**
     * @throws Exception
     */
    private function getCreateDate(object $info): DateTime
    {
        if (empty($info->printFormInfo)) {
            return new DateTime();
        }

        return match (true) {
            !empty($info->printFormInfo->publishDate) => new DateTime($info->printFormInfo->publishDate),
            !empty($info->printFormInfo->createDate) => new DateTime($info->printFormInfo->createDate),
            default => new DateTime()
        };
    }

    private function getDirection(): Direction
    {
        return $this->directionRepository->findOneBy([
            'code' => self::FZ44
        ]);
    }

    private function getControlOrganization(object $commonInfo): ControlOrganization
    {
        $organization = $commonInfo->registrationKO ?? $commonInfo->considerationKO;
        $organizationEntity = $this->findOrganizationByData($organization);
        if ($organizationEntity !== null) {
            return $organizationEntity;
        }

        return (new ControlOrganization())
            ->setArchive(true)
            ->setName($organization->fullName ?? $organization->shortName)
            ->setRegNum($organization->regNum)
            ->setConsRegistryNum($organization->consRegistryNum ?? null)
            ->setInn($organization->INN ?? null)
            ->setKpp($organization->KPP ?? null);
    }

    private function findOrganizationByData(object $organization): ?ControlOrganization
    {
        $searchBy = match (true) {
            !empty($organization->regNum) => [
                'regNum' => $organization->regNum
            ],
            !empty($organization->consRegistryNum) => [
                'consRegistryNum' => $organization->consRegistryNum
            ],
            !empty($organization->INN) && !empty($organization->KPP) => [
                'inn' => $organization->INN,
                'kpp' => $organization->KPP
            ],
            default => []
        };

        return !empty($searchBy) ? $this->controlOrganizationRepository->findOneBy($searchBy) : null;
    }

    private function getApplicantOrganization(object $applicant): Organization
    {
        [$inn, $kpp, $regNum, $name, $class] = match (true) {
            !empty($applicant->legalEntity) => [
                $applicant->legalEntity->INN ?? null,
                $applicant->legalEntity->KPP ?? null,
                null,
                $applicant->legalEntity->shortName ?? $applicant->legalEntity->fullName ?? null,
                LegalEntity::class
            ],
            !empty($applicant->individualBusinessman) => [
                $applicant->individualBusinessman->INN ?? null,
                null,
                null,
                $applicant->individualBusinessman->name ?? null,
                IndividualBusinessman::class
            ],
            !empty($applicant->individualPerson) => [
                $applicant->individualPerson->INN ?? null,
                null,
                null,
                $applicant->individualPerson->name ?? null,
                IndividualPerson::class
            ],
        };

        $organizationEntity = $this->findApplicantOrganization($inn, $kpp, $regNum, $name);
        if ($organizationEntity) {
            return $organizationEntity;
        }

        return (new $class())
            ->setName($name)
            ->setRegNum($regNum)
            ->setInn($inn)
            ->setKpp($kpp);
    }

    private function findApplicantOrganization(
        ?string $inn,
        ?string $kpp,
        ?string $regNum,
        ?string $name
    ): ?Organization {
        $searchBy = match (true) {
            $inn !== null && $kpp !== null => [
                'inn' => $inn,
                'kpp' => $kpp
            ],
            $inn !== null => [
                'inn' => $inn
            ],
            $regNum !== null => [
                'regNum' => $regNum
            ],
            $name !== null => [
                'name' => $name
            ],
            default => []
        };

        return !empty($searchBy) ? $this->organizationRepository->findOneBy($searchBy) : null;
    }

    private function getPurchase(object $purchase): Purchase
    {
        $purchaseEntity = $this->purchaseRepository->findOneBy([
            'purchaseNumber' => $purchase->purchaseNumber
        ]);

        if ($purchaseEntity !== null) {
            return $purchaseEntity;
        }

        return (new Purchase())
            ->setPurchaseName($purchase->purchaseName)
            ->setPurchaseNumber($purchase->purchaseNumber);
    }
}
