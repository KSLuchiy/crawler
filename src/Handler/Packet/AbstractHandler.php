<?php

namespace App\Handler\Packet;

use App\Entity\IntegrationPacket;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use stdClass;

abstract class AbstractHandler implements HandlerInterface
{
    protected string $packet = '';

    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritdoc
     */
    abstract public function process(IntegrationPacket $packet, array $data): void;

    /**
     * @inheritdoc
     */
    public function supports(string $name): bool
    {
        return $name === $this->packet;
    }

    protected function getFileExtension(string $name): string
    {
        $extensions = explode('.', $name);
        return array_pop($extensions);
    }

    /**
     * Временное решение т.к. номер не всегда присутствует в пакете
     */
    protected function getRegNumberFromFileName(string $name): ?string
    {
        return explode('_', $name)[1] ?? null;
    }

    /**
     * Рекурсивно превращаем массив в объект.
     */
    protected function arrayToObject(mixed $data): mixed
    {
        if (!is_array($data)) {
            return $data;
        } elseif (is_numeric(key($data))) {
            return array_map('self::arrayToObject', $data);
        } else {
            return (object) array_map('self::arrayToObject', $data);
        }
    }
}
