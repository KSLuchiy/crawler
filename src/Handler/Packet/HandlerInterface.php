<?php

namespace App\Handler\Packet;

use App\Entity\IntegrationPacket;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag(HandlerInterface::class)]
interface HandlerInterface
{
    /**
     * Проверка поддержки обработки выбранной стратегией.
     */
    public function supports(string $name): bool;

    /**
     * Обработка пакета выбранной стратегией.
     */
    public function process(IntegrationPacket $packet, array $data): void;
}
