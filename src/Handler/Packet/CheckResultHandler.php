<?php

namespace App\Handler\Packet;

use App\Entity\Complaint;
use App\Entity\ComplaintResult;
use App\Entity\ComplaintResult as ComplaintResultEntity;
use App\Entity\ComplaintStatus;
use App\Entity\ControlOrganization;
use App\Entity\Document;
use App\Entity\IntegrationPacket;
use App\Exception\PacketHandlerException;
use App\Repository\ComplaintRepository;
use App\Repository\ComplaintStatusRepository;
use App\Repository\ControlOrganizationRepository;
use App\Service\ParserDocumentService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Throwable;

class CheckResultHandler extends AbstractHandler
{
    private array $complaintStatusMap = [
        'VIOLATIONS' => 'COMPLAINT_VIOLATIONS',
        'NO_VIOLATIONS' => 'COMPLAINT_NO_VIOLATIONS',
        'COMPLAINT_PARTLY_VALID' => 'COMPLAINT_PARTLY_VALID',
        'COMPLAINT_VIOLATIONS' => 'COMPLAINT_VIOLATIONS',
        'COMPLAINT_NO_VIOLATIONS' => 'COMPLAINT_NO_VIOLATIONS',
        'COMPLAINT_NO_VIOLATIONS_LAW' => 'COMPLAINT_NO_VIOLATIONS_LAW',
    ];

    protected string $packet = 'checkResult';

    public function __construct(
        EntityManagerInterface $entityManager,
        private ComplaintRepository $complaintRepository,
        private ComplaintStatusRepository $complaintStatusRepository,
        private ControlOrganizationRepository $controlOrganizationRepository,
        private ParserDocumentService $parserDocumentService
    ) {
        parent::__construct($entityManager);
    }

    /**
     * @throws Throwable
     */
    public function process(IntegrationPacket $packet, array $data): void
    {
        $checkResultModel = $this->arrayToObject($data['export']['checkResult']);

        $this->entityManager->beginTransaction();
        try {
            $complaintEntity = (new ComplaintResultEntity())
                ->setCreatedAt($this->getCreateDate($checkResultModel->commonInfo))
                ->setReasonedAt($this->getReasonedDate($checkResultModel->commonInfo))
                ->setComplaint($this->getComplaint($checkResultModel, $packet->getName()))
                ->setControlSubjectOrganization($this->getControlSubjectOrganization($checkResultModel->commonInfo->owner))
                ->setStatus($this->getComplaintStatus($checkResultModel));

            $this->addAttachments($complaintEntity, $checkResultModel);

            $complaintEntity->getDocuments()->filter(function (Document $document) {
                if ($document->getStatus() === Document::STATUS_WAIT_CONFIRM) {
                    $document->setStatus(Document::STATUS_READY);
                }
            });

            $this->entityManager->persist($complaintEntity);
            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (Throwable $e) {
            $this->entityManager->rollback();
            throw new PacketHandlerException($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function getAttachmentsList(?object $checkType): array
    {
        $attachments = [];
        if ($checkType === null) {
            return $attachments;
        }

        if (!empty($checkType->decision->attachments)) {
            $attachments[] = [$checkType->decision->attachments, Document::TYPE_DECISION];
        }

        if (!empty($checkType->decisionPrescription->attachments)) {
            $attachments[] = [$checkType->decisionPrescription->attachments, null];
        }

        if (!empty($checkType->act->attachments)) {
            $attachments[] = [$checkType->act->attachments, null];
        }

        if (!empty($checkType->actPrescription->attachments)) {
            $attachments[] = [$checkType->actPrescription->attachments, null];
        }

        if (!empty($checkType->prescription->attachments)) {
            $attachments[] = [$checkType->prescription->attachments, Document::TYPE_PRESCRIPTION];
        }

        return $attachments;
    }

    private function addAttachments(ComplaintResultEntity $complaint, object $checkResult)
    {
        $checkType = $checkResult->complaint ?? null;

        if (!empty($checkResult->base)) {
            $checkType = match (true) {
                !empty($checkResult->base->plannedCheck) => $checkResult->base->plannedCheck,
                !empty($checkResult->base->unplannedCheck) => $checkResult->base->unplannedCheck,
                !empty($checkResult->base->unplannedCheckComplaint) => $checkResult->base->unplannedCheckComplaint,
                default => false
            };
        }

        if ($checkType == null) {
            return;
        }

        foreach ($this->getAttachmentsList($checkType) as [$attachmentsList, $type]) {
            $attachments = is_array($attachmentsList->attachment) ? $attachmentsList->attachment : [$attachmentsList->attachment];
            foreach ($attachments as $attachment) {
                if (!$this->parserDocumentService->getParser($this->getFileExtension($attachment->fileName))) {
                    continue;
                }

                $document = (new Document\ComplaintResultDocument())
                    ->setComplaintResult($complaint)
                    ->setName($attachment->docDescription)
                    ->setRealName($attachment->fileName)
                    ->setPublishedContentId($attachment->publishedContentId ?? null)
                    ->setUrl($attachment->url)
                    ->setType($type)
                    ->setStatus(Document::STATUS_WAIT_CONFIRM);

                $complaint->addDocument($document);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function getComplaintStatus(object $checkResult): ComplaintStatus
    {
        $codeStatus = $checkResult->complaint?->checkResult ?? $checkResult->complaint?->complaintResult ?? null;

        if (!empty($checkResult->base)) {
            $codeStatus = match (true) {
                !empty($checkResult->base->plannedCheck) => $checkResult->base->plannedCheck->checkResult,
                !empty($checkResult->base->unplannedCheck) => $checkResult->base->unplannedCheck->checkResult,
                !empty($checkResult->base->unplannedCheckComplaint) => $checkResult->base->unplannedCheckComplaint->checkResult,
                default => false
            };
        }

        if ($codeStatus === null) {
            throw new Exception("Complaint result not found");
        }

        $complaintStatus = $this->complaintStatusRepository->findOneBy([
            'code' => $this->complaintStatusMap[$codeStatus]
        ]);

        if ($complaintStatus === null) {
            throw new Exception("Complaint result not found");
        }

        return $complaintStatus;
    }

    /**
     * @throws Exception
     */
    private function getComplaint(object $checkResult, string $name): Complaint
    {
        $regNumber = $checkResult->complaint?->regNumber ?? null;

        if (!empty($checkResult->base)) {
            $regNumber = match (true) {
                !empty($checkResult->base->plannedCheck) => $checkResult->base->plannedCheck->checkNumber,
                !empty($checkResult->base->unplannedCheck) => $checkResult->base->unplannedCheck->regNumber,
                !empty($checkResult->base->unplannedCheckComplaint) => $checkResult->base->unplannedCheckComplaint->regNumber,
                default => false
            };
        }

        if (null === $regNumber) {
            $regNumber = $this->getRegNumberFromFileName($name);
        }

        $complaintNumber = $checkResult->complaint?->complaintNumber ?? null;
        if (null === $regNumber && null === $complaintNumber) {
            throw new Exception("Complaint number and reg number is empty.");
        }

        $complaint = $this->complaintRepository->getComplaintByRegNumberOrComplaintNumber($regNumber, $complaintNumber);
        if (null === $complaint) {
            throw new Exception("Complaint not found.");
        }

        return $complaint;
    }

    /**
     * @throws Exception
     */
    private function getCreateDate(object $info): DateTime
    {
        return !empty($info->createDate) ? new DateTime($info->createDate) : new DateTime();
    }

    /**
     * @throws Exception
     */
    private function getReasonedDate(object $info): DateTime
    {
        return !empty($info->publishDate) ? new DateTime($info->publishDate) : new DateTime();
    }

    private function getControlSubjectOrganization(object $organization): ControlOrganization
    {
        $inn = $organization->INN ?? null;
        $kpp = $organization->KPP ?? null;
        $regNum = $organization->regNum ?? null;
        $name = $organization->fullName ?? $organization->shortName ?? null;

        $organizationEntity = $this->findControlSubjectOrganization($inn, $kpp, $regNum, $name);
        if ($organizationEntity) {
            return $organizationEntity;
        }

        return (new ControlOrganization())
            ->setArchive(true)
            ->setName($name)
            ->setRegNum($regNum)
            ->setInn($inn)
            ->setKpp($kpp);
    }

    private function findControlSubjectOrganization(
        ?string $inn,
        ?string $kpp,
        ?string $regNum,
        ?string $name
    ): ?ControlOrganization {
        $searchBy = match (true) {
            $inn !== null && $kpp !== null => [
                'inn' => $inn,
                'kpp' => $kpp
            ],
            $inn !== null => [
                'inn' => $inn
            ],
            $regNum !== null => [
                'regNum' => $regNum
            ],
            $name !== null => [
                'name' => $name
            ],
            default => []
        };

        return !empty($searchBy) ? $this->controlOrganizationRepository->findOneBy($searchBy) : null;
    }
}
