<?php

namespace App\Handler;

use App\Entity\IntegrationPacket\PacketXml;
use Exception;
use Throwable;

interface PacketHandlerInterface
{
    /**
     * Обработка загруженного пакета.
     *
     * @throws Exception|Throwable
     */
    public function handle(PacketXml $packet): bool;
}
