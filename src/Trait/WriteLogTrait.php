<?php

namespace App\Trait;

use Symfony\Component\Console\Output\OutputInterface;

trait WriteLogTrait
{
    protected function write(OutputInterface $output, string $message): void
    {
        $output->writeln((new \DateTime())->format('Y-m-d H:i:s') . ' - ' . $message);
    }
}