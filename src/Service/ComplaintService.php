<?php

namespace App\Service;

use App\Dto\Request\ComplaintBody;
use App\Dto\Request\FasItemFilter;
use App\Dto\Request\FasItemFilter\SortingFilter;
use App\Entity\Complaint;
use App\Entity\Document;
use App\Repository\ComplaintStatusRepository;
use App\Transform\ResponseDtoTransform;
use DateTime;
use DateTimeInterface;
use Elastica\Aggregation\Terms as AggregationTerms;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MatchPhrase;
use Elastica\Query\MatchQuery;
use Elastica\Query\Range;
use Elastica\Query\Terms;
use Elastica\Query\Term;
use Exception;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class ComplaintService
{
    private const KEYWORDS = 'keywords';
    private const ID = 'id';
    private const DIRECTION = 'direction';
    private const TYPE = 'type.value';
    private const TYPE_AGGREGATION = 'type.id';
    private const APPLICANT_ORGANISATION = 'applicant_organisation';
    private const CONTROL_SUBJECT_ORGANISATION = 'control_subject_organisation';
    private const CONTROL_BODY_ARCHIVE = 'control_body.archive';
    private const CREATED_AT = 'created_at';
    private const REASONED_AT = 'reasoned_at';
    private const PREVIEW = 'preview';
    private const BODY = 'body';
    private const BODY_HTML = 'body_html';
    private const REGISTRY_NUMBER = 'registry_number';
    private const INN = 'inn';
    private const TITLE = 'title';
    private const DESC = 'desc';
    private const ASC = 'asc';

    /**
     * Особые поля, отдельная обработка
     */
    private const IGNORE_FIELDS = [
        self::KEYWORDS,
        self::DIRECTION,
        self::TYPE,
        self::APPLICANT_ORGANISATION,
        self::CONTROL_SUBJECT_ORGANISATION,
    ];

    public function __construct(
        private PaginatedFinderInterface $finder,
        private ResponseDtoTransform $transform,
        private ComplaintStatusRepository $complaintStatusRepository,
    ) {
    }

    public function convertHtmlToText(string $text): string
    {
        preg_match("/<body.*\/body>/s", $text, $matches);
        $text = $matches[0] ?? $text;
        return $this->removeHtmlEntity(trim(str_replace(["\t", "\r", "\n", "PHPWord"], " ", strip_tags($text))));
    }

    public function removeHtmlEntity(string $text): string
    {
        $text = preg_replace("/&#?[a-z0-9]+;/i", " ", $text);
        return preg_replace('/\s+/', ' ', $text);
    }

    /**
     * @throws ExceptionInterface
     */
    public function findByQuery(FasItemFilter $query): array
    {
        [$complaints, $totalCount, $statistics] = $this->getComplaintsByQuery($query);

        return array_merge(
            ['items' => $complaints],
            $statistics,
            ['count' => $totalCount]
        );
    }

    /**
     * @throws ExceptionInterface
     * @throws Exception
     */
    private function getComplaintsByQuery(FasItemFilter $query): array
    {
        $request = $this->getRequestObject($query)
            ->getResults($query->getOffset(), $query->getLimit());
        $complaints = $this->transform->transformArray(
            $this->processComplaints($query, $request->toArray()),
            'App\Dto\Response\Complaint[]'
        );

        $statistics = $this->getStatistics($request->getAggregations());
        $complaints = $query->getOrder()?->getShowType()
            ? $this->calcLimitByStatistics(
                $query->getLimit(),
                $query->getOffset(),
                $complaints,
                $statistics[$query->getOrder()->getShowType()]
            )
            : $complaints;

        return [
            $complaints,
            $request->getTotalHits(),
            $statistics
        ];
    }

    /**
     * @throws Exception
     */
    private function getRequestObject(FasItemFilter $requestQuery): PaginatorAdapterInterface
    {
        $boolQuery = $this->buildQuery($requestQuery);
        $query = Query::create($boolQuery);

        $query = $this->sortByQuery($query, $requestQuery->getOrder());
        $query = $this->addAggregation($query)
            ->setSource([
                'excludes' => [
                    self::BODY,
                    self::BODY_HTML
                ]
            ])
            ->setHighlight([
                'pre_tags' => [
                    '<b>'
                ],
                'post_tags' => [
                    '</b>'
                ],
                'fields' => [
                    self::BODY => [
                        'type' => 'plain',
                        'fragment_size' => 150,
                        'number_of_fragments' => 5,
                        'force_source' => true,
                        'fragmenter' => 'span',
                        'no_match_size' => 150,
                    ]
                ]
            ])
            ->setTrackTotalHits(true);

        return $this->finder->createRawPaginatorAdapter($query);
    }

    private function buildQuery(FasItemFilter $query): BoolQuery
    {
        $boolQuery = new BoolQuery();

        // Исключаем из выборки архивные организации.
        $boolQuery
            ->addMust((new Term())->setTerm(self::CONTROL_BODY_ARCHIVE, false));

        if (!$query->getSearchForm()) {
            return $boolQuery;
        }

        foreach ($query->getSearchForm()->toArray() as $key => $item) {
            if ($item === null || $item instanceof DateTime || in_array($key, self::IGNORE_FIELDS)) {
                continue;
            }

            $boolQuery->addMust((new MatchQuery())->setFieldQuery($key, $item));
        }

        $this->buildQueryByDate(
            $boolQuery,
            self::CREATED_AT,
            $query->getSearchForm()->getCreateDateFrom(),
            $query->getSearchForm()->getCreateDateTo()
        );

        $this->buildQueryByDate(
            $boolQuery,
            self::REASONED_AT,
            $query->getSearchForm()->getReasonDateFrom(),
            $query->getSearchForm()->getReasonDateTo()
        );

        if ($query->getSearchForm()->getKeywords()) {
            $this->buildQueryByKeywords(
                $boolQuery,
                $query->getSearchForm()->getKeywords(),
                (bool)$query->getOrder()?->getSearchPhrase()
            );
        }

        if ($query->getSearchForm()->getDirection()) {
            $this->buildQueryByDirection($boolQuery, $query->getSearchForm()->getDirection());
        }

        if ($query->getSearchForm()->getType()) {
            $this->buildQueryByType($boolQuery, $query->getSearchForm()->getType());
        }

        if ($query->getSearchForm()->getApplicantOrganisation()) {
            $this->buildQueryByOrganization(
                $boolQuery,
                self::APPLICANT_ORGANISATION,
                $query->getSearchForm()->getApplicantOrganisation()
            );
        }

        if ($query->getSearchForm()->getControlSubjectOrganisation()) {
            $this->buildQueryByOrganization(
                $boolQuery,
                self::CONTROL_SUBJECT_ORGANISATION,
                $query->getSearchForm()->getControlSubjectOrganisation()
            );
        }

        return $boolQuery;
    }

    private function buildQueryByDate(
        BoolQuery $boolQuery,
        string $conditionName,
        ?DateTime $dateFrom,
        ?DateTime $dateTo
    ) {
        $condition = [];
        if ($dateFrom) {
            $condition['gte'] = $dateFrom->format(DateTimeInterface::ATOM);
        }
        if ($dateTo) {
            $condition['lte'] = $dateTo->format(DateTimeInterface::ATOM);
        }

        if (!$condition) {
            return;
        }

        $boolQuery->addFilter((new Range())->addField($conditionName, $condition));
    }

    private function buildQueryByKeywords(
        BoolQuery $boolQuery,
        string $text,
        bool $searchPhrase,
        string $field = self::BODY
    ): void {
        if ($this->isRegistryNumber($text)) {
            $boolQuery->addMust((new MatchPhrase())->setFieldQuery(self::REGISTRY_NUMBER, $text));
            return;
        }

        if ($searchPhrase) {
            $boolQuery->addMust((new MatchPhrase())->setFieldQuery($field, $text));
            return;
        }

        $text = explode(' ', $text);

        $phraseQuery = new BoolQuery();
        foreach ($text as $phrase) {
            $matchPhrase = (new MatchPhrase())
                ->setFieldAnalyzer($field, 'jmorphy')
                ->setFieldQuery($field, $phrase);
            $phraseQuery->addMust($matchPhrase);
        }
        $boolQuery->addMust($phraseQuery);
    }

    private function isRegistryNumber(string $phrase): bool
    {
        return (bool) preg_match('/^\b\d{18}\b$/', $phrase);
    }

    private function buildQueryByDirection(BoolQuery $boolQuery, string $text)
    {
        $tags = explode('|', $text);
        $tagsQuery = new Terms(self::DIRECTION, array_map('trim', $tags));
        $boolQuery->addMust($tagsQuery);
    }

    private function buildQueryByType(BoolQuery $boolQuery, string $text)
    {
        $query = new BoolQuery();

        $tags = explode('|', $text);
        foreach (array_map('trim', $tags) as $item) {
            $query->addShould((new MatchQuery())->setFieldQuery(self::TYPE, $item));
        }

        $boolQuery->addMust($query);
    }

    private function buildQueryByOrganization(BoolQuery $boolQuery, string $field, string $text)
    {
        $boolQuery
            ->addMust((new MatchQuery())->setFieldQuery(
                $field . '.' . (is_numeric($text) ? self::INN : self::TITLE),
                $text
            ));
    }

    /**
     * @throws Exception
     */
    private function sortByQuery(Query $query, ?SortingFilter $order): Query
    {
        if ($order && $order->getShowType()) {
            $query = $this->sortByType($query, $order);
        }

        return $this->sortByDate($query, $order);
    }

    /**
     * @throws Exception
     */
    private function sortByType(Query $query, SortingFilter $order): Query
    {
        $complaintStatus = $this->complaintStatusRepository->findOneBy(['code' => $order->getShowType()]);
        if (!$complaintStatus) {
            throw new Exception('Invalid complaint type');
        }

        return $this->sortByScript(
            $query,
            "doc['type.id'].value == " . $complaintStatus->getId() . " ? 1 : 0",
            "number",
            "DESC"
        );
    }

    private function sortByScript(Query $query, string $script, string $type, string $order): Query
    {
        return $query->addSort([
            '_script' => [
                'script' => $script,
                'type' => $type,
                'order' => $order,
            ]
        ]);
    }

    private function sortByDate(Query $query, ?SortingFilter $order): Query
    {
        if (
            !$order || !$order->getField() || !$order->getOrder() ||
            !in_array($order->getField(), [self::CREATED_AT, self::REASONED_AT]) ||
            !in_array($order->getOrder(), [self::ASC, self::DESC])
        ) {
            return $query->addSort([self::CREATED_AT => ['order' => self::DESC]]);
        }

        return $query->addSort([$order->getField() => ['order' => $order->getOrder()]]);
    }

    private function addAggregation(Query $query): Query
    {
        return $query->addAggregation(
            (new AggregationTerms(self::TYPE_AGGREGATION))
                ->setField(self::TYPE_AGGREGATION)
        );
    }

    private function processComplaints(FasItemFilter $query, array $complaintsData): array
    {
        foreach ($complaintsData as $key => $data) {
            if (!$query->getSearchForm()?->getKeywords()) {
                continue;
            }

            $complaintsData[$key]['source'][self::PREVIEW] = $this->createFieldPreview($data['highlight']);
        }

        return array_map(
            static function (array $data) {
                return $data['source'];
            },
            $complaintsData
        );
    }

    private function createFieldPreview(array $highlight): string
    {
        if ($highlight[self::BODY] === []) {
            return '';
        }

        return '...' . implode('...', $highlight[self::BODY]) . '...';
    }

    private function getStatistics(array $aggregation): array
    {
        $typeList = $this->complaintStatusRepository->findAllComplaintStatusCode();
        $typesCount = array_fill_keys(array_keys(array_flip($typeList)), 0);

        foreach ($aggregation[self::TYPE_AGGREGATION]['buckets'] as $type) {
            if (empty($typeList[$type['key']])) {
                continue;
            }

            $typesCount[$typeList[$type['key']]] = $type['doc_count'];
        }

        return $typesCount;
    }

    private function calcLimitByStatistics(int $limit, int $offset, array $complaints, int $statistic): array
    {
        $maxItems = min($statistic - $offset - $limit - 1, $limit);
        if ($maxItems < 0) {
            $maxItems = $limit;
        }
        return array_slice($complaints, 0, $maxItems);
    }

    /**
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function findById(int $id): array
    {
        $request = $this->finder->findRaw(
            $this->buildQueryById($id)
        );

        if (empty($request)) {
            return [];
        }

        $complaints = reset($request)->getData();
        unset($complaints['body']);

        return $this->transform->transformArray(
            [$complaints],
            'App\Dto\Response\Complaint[]'
        );
    }

    private function buildQueryById(int $id): Query
    {
        $boolQuery = new BoolQuery();

        // Исключаем из выборки архивные организации.
        $boolQuery
            ->addMust((new Term())->setTerm(self::ID, $id))
            ->addMust((new Term())->setTerm(self::CONTROL_BODY_ARCHIVE, false));

        return Query::create($boolQuery);
    }

    public function getComplaintBody(Complaint $complaint, ComplaintBody $complaintBody): ?string
    {
        if ($complaint->getComplaintResult()->count() === 0) {
            return null;
        }

        /** @var Document $document */
        $document = $complaint->getComplaintResult()->first()->getDocuments()
            ->filter(
                static function (Document $document) {
                    return $document->getType() === Document::TYPE_DECISION &&
                        $document->getStatus() === Document::STATUS_PROCESSED;
                }
            )->first();

        if (!$document || empty($document->getContent())) {
            return null;
        }

        return !empty($complaintBody->getKeywords())
            ? $this->getComplaintDocumentWithKeywords($complaintBody)
            : $document->getContent();
    }

    private function getComplaintDocumentWithKeywords(ComplaintBody $complaintBody): ?string
    {
        $request = $this->getSingleRequestObject($complaintBody)
            ->getResults(0, 1);

        $complaints = $request->toArray();
        $complaint = reset($complaints);
        if (empty($complaint)) {
            return null;
        }

        $text = reset($complaint['highlight'][self::BODY_HTML]);
        if(empty($text)) {
            return null;
        }

        return str_replace(PHP_EOL, '', $text);
    }

    private function getSingleRequestObject(ComplaintBody $requestQuery): PaginatorAdapterInterface
    {
        $boolQuery = $this->buildSingleQuery($requestQuery);
        $query = Query::create($boolQuery);

        $query = $this->addAggregation($query)
            ->setSource([
                'excludes' => [
                    self::BODY
                ]
            ])
            ->setHighlight([
                'pre_tags' => [
                    '<span style="font-size: 13pt; background-color: #009af1">'
                ],
                'post_tags' => [
                    '</span>'
                ],
                'fields' => [
                    self::BODY_HTML => [
                        'type' => 'plain',
                        'number_of_fragments' => 0,
                    ]
                ]
            ]);

        return $this->finder->createRawPaginatorAdapter($query);
    }

    private function buildSingleQuery(ComplaintBody $query): BoolQuery
    {
        $boolQuery = new BoolQuery();

        // Исключаем из выборки архивные организации.
        $boolQuery
            ->addMust((new Term())->setTerm(self::ID, $query->getId()))
            ->addMust((new Term())->setTerm(self::CONTROL_BODY_ARCHIVE, false));

        if ($query->getKeywords()) {
            $this->buildQueryByKeywords(
                $boolQuery,
                $query->getKeywords(),
                (bool)$query->getSearchPhrase(),
                self::BODY_HTML
            );
        }

        return $boolQuery;
    }
}
