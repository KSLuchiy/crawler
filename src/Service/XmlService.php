<?php

namespace App\Service;

use DOMDocument;
use DOMElement;
use DOMNode;
use DOMText;

class XmlService implements XmlServiceInterface
{
    private const PACKET_LIST = [
        'complaint',
        'checkResult'
    ];

    /**
     * @inheritdoc
     */
    public function xmlToArray(string $xml): array
    {
        $document = new DOMDocument();
        $document->loadXML($xml);

        return $this->nodeToArray($document);
    }


    /**
     * @inheritdoc
     */
    public function getPacketInfo(string $xml): array
    {
        $document = new DOMDocument();
        $document->loadXML($xml);

        foreach (self::PACKET_LIST as $item) {
            $packetBlock = $document->getElementsByTagName($item);
            if ($packetBlock->count() === 0) {
                continue;
            }

            /** @var DOMElement $packetBlock */
            $packetBlock = $packetBlock[0];
            if ($packetBlock->getAttribute("schemeVersion") === '') {
                continue;
            }

            return [$packetBlock->getAttribute("schemeVersion"), $item];
        }

        return [null, null];
    }

    private function nodeToArray(DOMNode $node): array|string|null
    {
        if ($node->nodeType instanceof DOMText) {
            return null;
        }

        if ($node->hasChildNodes()) {
            if ($node->childNodes->length === 1 && $node->childNodes->item(0) instanceof DOMText) {
                return $node->nodeValue;
            }

            $result = [];

            foreach ($node->childNodes as $childNode) {
                if ($childNode->nodeType === XML_ELEMENT_NODE) {
                    $nodeValue = $this->nodeToArray($childNode);

                    if ($nodeValue !== null) {
                        if ($nodeValue === 'true') {
                            $nodeValue = true;
                        } elseif ($nodeValue === 'false') {
                            $nodeValue = false;
                        }

                        $key = preg_replace('/^.+:/', '', $childNode->nodeName);
                        $result[$key][] = $nodeValue;
                    }
                }
            }

            // нормализация
            foreach (array_keys($result) as $key) {
                if (count($result[$key]) === 1) {
                    $result[$key] = $result[$key][0];
                }
            }

            return $result;
        }

        return $node->nodeValue;
    }
}
