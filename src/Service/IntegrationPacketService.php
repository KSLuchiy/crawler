<?php

namespace App\Service;

use App\Entity\IntegrationPacket;
use App\Entity\IntegrationPacket\PacketXml;
use App\Entity\IntegrationPacket\PacketZip;
use App\Process\ProcessPool;
use App\Repository\IntegrationPacketRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Component\Process\Process;
use Symfony\Component\Uid\Uuid;

class IntegrationPacketService
{
    public const PACKET_PRIORITY = [
        'complaint' => 0,
        'checkResult' => 1,
    ];

    public function __construct(
        private EntityManagerInterface $entityManager,
        private IntegrationPacketRepository $integrationPacketRepository,
        private ProcessPool $processPool,
        private FtpService $ftpService,
        private ZipService $zipService,
        private FileService $fileService
    ) {
    }

    /**
     * @throws Exception
     */
    public function sync(string $directory, ?string $minDate): void
    {
        foreach ($this->ftpService->getFiles($directory) as $source) {
            $command = [
                '/usr/local/bin/php',
                __DIR__ . '/../../bin/console',
                'app:download-integration-packet',
                '--source=' . $source
            ];

            if ($minDate) {
                $command[] = '--min-date=' . $minDate;
            }

            $process = new Process($command);
            $this->processPool->append($process);
        }

        $this->processPool->start();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function download(string $source, ?DateTime $minDate): void
    {
        $fileName = basename($source);
        $modifyDate = $this->ftpService->getModifyDate($source);

        if ($minDate && $modifyDate < $minDate || strrpos($fileName, ".") === false) {
            return;
        }

        $packetZip = $this->integrationPacketRepository->findOneByName(
            $fileName,
            PacketZip::DISCRIMINATOR
        );

        if ($packetZip && $packetZip->getModifyDate() <= $modifyDate) {
            return;
        }

        if (!$packetZip) {
            $packetZip = new PacketZip();
            $packetZip
                ->setGuid(Uuid::v4())
                ->setName(basename($source))
                ->setPath($packetZip->getDirName() . $fileName)
                ->setSize(0);
        }

        $packetZip
            ->setStatus(IntegrationPacket::STATUS_READY)
            ->setModifyDate($modifyDate);

        if ($this->fileService->saveFileFromContent($packetZip->getPath(), $this->ftpService->getContent($source))) {
            $fileInfo = $this->fileService->getFileInfo($packetZip->getPath());

            $packetZip
                ->setHash($this->fileService->getFileHash($fileInfo->getRealPath()))
                ->setSize($fileInfo->getSize())
                ->setStatus(IntegrationPacket::STATUS_SUCCESS);
        } else {
            $packetZip->setStatus(IntegrationPacket::STATUS_ERROR);
        }

        $this->save($packetZip);
    }

    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function unzip(PacketZip $packetZip): bool
    {
        $fileInfo = $this->fileService->getFileInfo($packetZip->getPath());

        if (!$this->zipService->openZip($fileInfo->getRealPath())) {
            $packetZip->setStatus(IntegrationPacket::STATUS_ERROR_UNPACKED);

            $this->save($packetZip);

            return false;
        }

        $dirName = strtok($packetZip->getName(), '.') . DIRECTORY_SEPARATOR;

        foreach ($this->zipService->getZipFile() as $fileName => $content) {
            if (preg_match('/\.xml$/si', $fileName) !== 1) {
                continue;
            }

            $packetXml = $this->integrationPacketRepository->findOneByName(
                $fileName,
                PacketXml::DISCRIMINATOR
            );

            if ($packetXml && $packetXml->getModifyDate() <= $packetZip->getModifyDate()) {
                continue;
            }

            if (!$packetXml) {
                $packetXml = new PacketXml();
                $packetXml
                    ->setGuid(Uuid::v4())
                    ->setName($fileName)
                    ->setPriority($this->getPriority($fileName))
                    ->setPath($packetXml->getDirName() . $dirName . $fileName)
                    ->setSize(0);
            }

            $packetXml->setStatus(IntegrationPacket::STATUS_READY);

            if ($this->fileService->saveFileFromContent($packetXml->getPath(), $content)) {
                $fileInfo = $this->fileService->getFileInfo($packetXml->getPath());

                $packetXml
                    ->setSize($fileInfo->getSize())
                    ->setHash($this->fileService->getFileHash($fileInfo->getRealPath()))
                    ->setStatus(IntegrationPacket::STATUS_SUCCESS)
                    ->setModifyDate((new DateTime())->setTimestamp($fileInfo->getCTime()));
            } else {
                $packetXml->setStatus(IntegrationPacket::STATUS_ERROR);
            }

            $this->save($packetXml);
        }

        $packetZip->setStatus(IntegrationPacket::STATUS_UNPACKED);

        $this->save($packetZip);

        $this->zipService->closeZip();

        return true;
    }

    public function save(IntegrationPacket $packet): void
    {
        $this->entityManager->persist($packet);
        $this->entityManager->flush();
    }

    public function lockPacket(IntegrationPacket $packet): void
    {
        $packet
            ->setStatus(IntegrationPacket::STATUS_PROCESS)
            ->setModifyDate(new DateTime());

        $this->save($packet);
    }

    private function getPriority(string $fileName): int
    {
        return self::PACKET_PRIORITY[explode('_', $fileName)[0]] ?? IntegrationPacket::MAX_VALUE_PRIORITY;
    }
}
