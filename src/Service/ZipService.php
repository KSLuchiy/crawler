<?php

namespace App\Service;

use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;
use Psr\Log\LoggerInterface;

class ZipService
{
    /**
     * @var ZipFile
     */
    private ZipFile $zipFile;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param ZipFile $zipFile
     * @param LoggerInterface $logger
     */
    public function __construct(
        ZipFile $zipFile,
        LoggerInterface $logger
    ) {
        $this->zipFile = $zipFile;
        $this->logger = $logger;
    }

    /**
     * @return ZipFile
     */
    public function getZipFile(): ZipFile
    {
        return $this->zipFile;
    }

    /**
     * @param string $fileName
     * @return bool
     */
    public function openZip(string $fileName): bool
    {
        try {
            $this->zipFile->openFile($fileName);

            return true;
        } catch (ZipException $zipException) {
            $this->logger->error($zipException->getMessage(), $zipException->getTrace());

            return false;
        }
    }

    public function closeZip(): void
    {
        $this->zipFile->close();
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        return $this->zipFile->getListFiles();
    }
}