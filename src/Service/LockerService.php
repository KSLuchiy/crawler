<?php

namespace App\Service;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;

class LockerService
{
    public function __construct(public CacheItemPoolInterface $cache)
    {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function lock(string $key, int $id): void
    {
        $key = $this->filterChars($key);

        $item = $this->cache->getItem($key);
        $lookIds = $item->get() ?? [];

        $pos = array_search($id, $lookIds);
        if ($pos !== false) {
            return;
        }
        $lookIds[] = $id;

        $item->set($lookIds);
        $this->cache->save($item);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function unlock(string $key, int $id): void
    {
        $key = $this->filterChars($key);
        if (!$this->cache->hasItem($key)) {
            return;
        }

        $item = $this->cache->getItem($key);
        $lookIds = $item->get();

        $pos = array_search($id, $lookIds);
        if ($pos === false) {
            return;
        }
        unset($lookIds[$pos]);

        $item->set($lookIds);
        $this->cache->save($item);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function isLocked(string $key, int $id): bool
    {
        $key = $this->filterChars($key);
        if (!$this->cache->hasItem($key)) {
            return false;
        }

        return in_array($id, $this->cache->getItem($key)->get());
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getLookedIds(string $key): array
    {
        $key = $this->filterChars($key);
        if (!$this->cache->hasItem($key)) {
            return [];
        }

        $data = $this->cache->getItem($key)->get();
        return is_array($data) ? $data : [];
    }

    private function filterChars(string $key): string
    {
        return str_replace(['{', '}', '(', ')', '/', '\\', '@', ':'], '.', $key);
    }
}
