<?php

namespace App\Service;

use App\Parser\ParserInterface;

class ParserDocumentService
{
    public function __construct(private iterable $parsers)
    {
    }

    public function getParser(string $extension): ?ParserInterface
    {
        foreach ($this->parsers as $parser) {
            if ($parser instanceof ParserInterface && $parser->supports($extension)) {
                return $parser;
            }
        }

        return null;
    }
}