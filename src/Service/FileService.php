<?php

namespace App\Service;

use Psr\Log\LoggerInterface;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\SplFileInfo;

class FileService
{
    /**
     * @var array
     */
    private array $parameters;

    /**
     * @var Filesystem
     */
    private Filesystem $filesystem;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param ParameterBagInterface $parameterBag
     * @param Filesystem $filesystem
     * @param LoggerInterface $logger
     */
    public function __construct(
        ParameterBagInterface $parameterBag,
        Filesystem $filesystem,
        LoggerInterface $logger
    ) {
        $this->parameters = $parameterBag->get('file_service');
        $this->filesystem = $filesystem;
        $this->logger = $logger;
    }

    /**
     * @param string $fileName
     * @param mixed $content
     * @return bool
     */
    public function saveFileFromContent(string $fileName, mixed $content): bool
    {
        try {
            $this->filesystem->dumpFile($this->parameters['directory'] . $fileName, $content);

            return true;
        } catch (IOException $IOException) {
            $this->logger->error($IOException->getMessage(), $IOException->getTrace());

            return false;
        }
    }

    /**
     * @param string $fileName
     * @return SplFileInfo
     */
    public function getFileInfo(string $fileName): SplFileInfo
    {
        return new SplFileInfo($this->parameters['directory'] . $fileName, '', '');
    }

    /**
     * @param string $fileName
     * @return bool|string
     */
    public function getFileHash(string $fileName): bool|string
    {
        return hash_file('sha256', $fileName);
    }
}