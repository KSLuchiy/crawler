<?php

namespace App\Service;

use App\Entity\Complaint;
use App\Entity\Document;
use App\Entity\Document\ComplaintDocument;
use App\Entity\Document\ComplaintResultDocument;
use App\Parser\ParserInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\ElasticaBundle\Persister\ObjectPersister;
use Throwable;

class DocumentService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private HttpService $httpService,
        private FileService $fileService,
        private ParserDocumentService $parserDocumentService,
        private ObjectPersister $complaintPersister
    ) {
    }

    public function download(Document $document): bool
    {
        $fileName = sprintf('%s.%s', (new DateTime())->format('U'),
            pathinfo($document->getRealName(), PATHINFO_EXTENSION));

        $document
            ->setName($fileName)
            ->setPath($document->getDirName() . $fileName);

        $response = $this->httpService->get($document->getUrl());

        if ($response && $this->fileService->saveFileFromContent($document->getPath(), $this->httpService->getContent($response))) {
            $document->setStatus(Document::STATUS_SUCCESS);
        } else {
            $document->setStatus(Document::STATUS_ERROR);
        }

        $this->save($document);

        return $document->getStatus() === Document::STATUS_SUCCESS;
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function parse(Document $document): bool
    {
        $fileInfo = $this->fileService->getFileInfo($document->getPath());
        $parser = $this->parserDocumentService->getParser($fileInfo->getExtension());

        if (!$parser instanceof ParserInterface) {
            $document->setStatus(Document::STATUS_ERROR);
            $this->save($document);
            throw new Exception(sprintf('%s files are not supported', $fileInfo->getExtension()));
        }

        try {
            if (!($content = $parser->parse($fileInfo))) {
                throw new Exception("Cant parse file.");
            }
            $document
                ->setContent($content)
                ->setStatus(Document::STATUS_PROCESSED);
        } catch (Throwable $e) {
            $document
                ->setContent($e->getMessage())
                ->setStatus(Document::STATUS_ERROR_PROCESSED);

            $this->save($document);
            throw $e;
        }

        $this->save($document);

        return $document->getStatus() === Document::STATUS_PROCESSED;
    }

    public function save(Document $document): void
    {
        $this->entityManager->persist($document);
        $this->entityManager->flush();
    }

    public function updateComplaintInfo(Document $document)
    {
        $complaintResult = match (true) {
            $document instanceof ComplaintResultDocument => $document->getComplaintResult(),
            $document instanceof ComplaintDocument => $document->getComplaint()->getComplaintResult()->first(),
            default => null
        };

        if (!$complaintResult) {
            return;
        }

        $this->complaintPersister->replaceOne($complaintResult);
    }

    /**
     * @throws Throwable
     */
    public function tryFindComplaintBody(Complaint $complaint): void
    {
        if ($complaint->getComplaintResult()->count() === 0) {
            return;
        }

        $documents = $complaint->getComplaintResult()->first()->getDocuments()
            ->filter(
                static function (Document $document) {
                    return $document->getType() === Document::TYPE_DECISION &&
                        $document->getStatus() !== Document::STATUS_PROCESSED;
                }
            );

        foreach ($documents as $document) {
            try {
                $this->processFindComplaintBody($document);
            } catch (Throwable) {
                // todo Придумать замену для ручного поиска контента документов
            }
        }
    }

    /**
     * @throws Throwable
     */
    public function processFindComplaintBody(Document $document)
    {
        try {
            if (!$this->download($document)) {
                throw new Exception(sprintf('Error download document: %d', $document->getId()));
            }
        } catch (Throwable $e) {
            $document->setStatus(Document::STATUS_ERROR);
            $this->save($document);

            throw $e;
        }

        try {
            if (!$this->parse($document)) {
                throw new Exception(sprintf('Error parse document: %d', $document->getId()));
            }
        } catch (Throwable $e) {
            $document->setStatus(Document::STATUS_ERROR_PROCESSED);
            $this->save($document);

            throw $e;
        }

        $this->updateComplaintInfo($document);
    }
}
