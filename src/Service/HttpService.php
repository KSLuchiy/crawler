<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class HttpService
{
    /**
     * @var Client
     */
    private Client $client;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $client,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param string $url
     * @return ResponseInterface|null
     */
    public function get(string $url): ?ResponseInterface
    {
        try {
            return $this->client->request('GET', $url, ['stream' => true]);
        } catch (GuzzleException $guzzleException) {
            $this->logger->error($guzzleException->getMessage(), $guzzleException->getTrace());

            return null;
        }
    }

    /**
     * @param ResponseInterface $response
     * @return string|null
     */
    public function getContent(ResponseInterface $response): ?string
    {
        return $response->getBody()->getContents();
    }

    /**
     * @param ResponseInterface $response
     * @return string|null
     */
    public function getRealName(ResponseInterface $response): ?string
    {
        if (preg_match('/.*?filename="(.+?)"/', $response->getHeaderLine('Content-Disposition'), $matches)) {
            return rawurldecode($matches[1]);
        }

        if (preg_match('/.*?filename=([^; ]+)/', $response->getHeaderLine('Content-Disposition'), $matches)) {
            return rawurldecode($matches[1]);
        }

        return null;
    }
}
