<?php

namespace App\Service;

use DateTime;
use FtpClient\FtpClient;
use FtpClient\FtpException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Throwable;

class FtpService
{
    /**
     * @var array
     */
    private array $parameters;

    /**
     * @var FtpClient
     */
    private FtpClient $client;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param ParameterBagInterface $parameterBag
     * @param FtpClient $client
     * @param LoggerInterface $logger
     */
    public function __construct(
        ParameterBagInterface $parameterBag,
        FtpClient $client,
        LoggerInterface $logger
    ) {
        $this->parameters = $parameterBag->get('ftp_service');
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @return FtpClient
     * @throws FtpException
     */
    private function getClient(): FtpClient
    {
        $this->client->connect($this->parameters['host']);
        $this->client->login($this->parameters['user'], $this->parameters['password']);
        $this->client->pasv(true);

        return $this->client;
    }

    /**
     * @param string $fileName
     * @return string|null
     */
    public function getContent(string $fileName): ?string
    {
        try {
            return $this->getClient()->getContent($fileName);
        } catch (FtpException $ftpException) {
            $this->logger->error($ftpException->getMessage(), $ftpException->getTrace());

            return null;
        }
    }

    /**
     * @param string $directory
     * @return array
     */
    public function getFiles(string $directory): array
    {
        try {
            return $this->getClient()->nlist($directory);
        } catch (FtpException $ftpException) {
            $this->logger->error($ftpException->getMessage(), $ftpException->getTrace());

            return [];
        }
    }

    /**
     * @param string $fileName
     * @param string $format
     * @return DateTime
     */
    public function getModifyDate(string $fileName, string $format = 'Y-m-d H:i:s'): DateTime
    {
        try {
            $modifiedTime = $this->getClient()->modifiedTime($fileName, $format);

            return new DateTime($modifiedTime);
        } catch (Throwable $throwable) {
            $this->logger->error($throwable->getMessage(), $throwable->getTrace());

            return new DateTime();
        }
    }
}