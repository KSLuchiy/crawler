<?php

namespace App\Service;

use Exception;

interface XmlServiceInterface
{
    /**
     * Получение версии, типа пакета.
     */
    public function getPacketInfo(string $xml): array;

    /**
     * Преобразовать xml в читаемый массив.
     *
     * @throws Exception
     */
    public function xmlToArray(string $xml): array;
}
