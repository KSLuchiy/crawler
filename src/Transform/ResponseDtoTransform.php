<?php

namespace App\Transform;

use App\Dto\DtoInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class ResponseDtoTransform extends DtoTransform
{
    public function transform(
        mixed $data,
        string $className,
        string $format = 'json',
        array $context = []
    ): DtoInterface|array {
        $object = $this->deserialize($data, $className, $format, $context);
        return $this->validate($object);
    }

    /**
     * @throws ExceptionInterface
     */
    public function transformArray(
        mixed $data,
        string $className,
        string $format = 'json',
        array $context = []
    ): DtoInterface|array {
        $object = $this->denormalize($data, $className, $format, $context);
        return $this->validate($object);
    }
}
