<?php

namespace App\Transform;

use App\Dto\ConstructRequestObjectInterface;
use App\Dto\DtoInterface;
use App\Dto\QueryObjectInterface;
use App\Dto\RequestBodyObjectInterface;
use App\Dto\RequestObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class RequestDtoTransform extends DtoTransform
{
    /**
     * @throws ExceptionInterface
     */
    public function transform(Request $request, string $className, string $format, array $context = []): DtoInterface
    {
        if (is_a($className, QueryObjectInterface::class, true)) {
            $object = $this->denormalize($request->query->all(), $className, $format, $context);
            $object = $this->validate($object);
        } elseif (is_a($className, RequestObjectInterface::class, true)) {
            $data = $request->isMethod('GET') || $request->isMethod('HEAD')
                ? array_merge($request->attributes->get('_route_params', []), $request->query->all())
                : $this->getDataFromRequest($request);

            $object = $this->denormalize($data, $className, $format, $context);
            $object = $this->validate($object);
        } elseif (is_a($className, RequestBodyObjectInterface::class, true)) {
            $object = $this->denormalize($request->getContent(), $className, $format, $context);
            $object = $this->validate($object);
        } elseif (is_a($className, ConstructRequestObjectInterface::class, true)) {
            $object = new $className($request);
            $object = $this->validate($object);
        } else {
            throw new \RuntimeException('Class ' . $className . ' is not supported.');
        }

        return $object;
    }

    private function getDataFromRequest(Request $request): array
    {
        return $request->getContentType() === 'json'
            ? json_decode($request->getContent(), true)
            : $request->request->all();
    }
}
