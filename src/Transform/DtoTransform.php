<?php

namespace App\Transform;

use App\Dto\DtoInterface;
use App\Exception\DtoValidationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class DtoTransform
{
    protected SerializerInterface $serializer;
    protected ValidatorInterface $validator;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @throws ExceptionInterface
     */
    protected function denormalize(
        mixed $data,
        string $className,
        string $format = 'json',
        array $context = []
    ): DtoInterface|array {
        $context += [
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
        ];

        return $this->serializer->denormalize($data, $className, $format, $context);
    }

    protected function deserialize(
        mixed $data,
        string $className,
        string $format = 'json',
        array $context = []
    ): DtoInterface|array {
        $context += [
            AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true,
        ];

        return $this->serializer->deserialize($data, $className, $format, $context);
    }

    protected function validate(DtoInterface|array $object): DtoInterface|array
    {
        $errors = [];

        if (!is_array($object)) {
            $errors = $this->validator->validate($object);
        } else {
            foreach ($object as $item) {
                $errors = $this->validator->validate($item);
                if (count($errors)) {
                    throw new DtoValidationException($errors);
                }
            }
        }

        if (count($errors)) {
            throw new DtoValidationException($errors);
        }

        return $object;
    }
}
