<?php

namespace App\Transform;

use App\Entity\Complaint;
use App\Entity\ComplaintResult;
use App\Entity\Document;
use App\Entity\Document\ComplaintResultDocument;
use App\Service\ComplaintService;
use DateTimeInterface;
use Elastica\Document as ElasticaDocument;
use FOS\ElasticaBundle\Transformer\ModelToElasticaTransformerInterface;

class ElasticaComplaintToArrayTransformer implements ModelToElasticaTransformerInterface
{
    private const COMPLAINT_LINK = 'https://zakupki.gov.ru/epz/complaint/card/complaint-information.html?id=';

    public function __construct(private ComplaintService $complaintService)
    {
    }

    public function transform(object $object, array $fields): ElasticaDocument
    {
        /** @var ComplaintResult $object */
        return new ElasticaDocument($object->getId(), $this->getData($object));
    }

    private function getData(ComplaintResult $object): array
    {
        return [
            'id' => $object->getComplaint()->getId(),
            'created_at' => $object->getComplaint()->getCreatedAt()->format(DateTimeInterface::ATOM),
            'updated_at' => $object->getComplaint()->getUpdatedAt()->format(DateTimeInterface::ATOM),
            'reasoned_at' => $object->getReasonedAt()->format(DateTimeInterface::ATOM),
            'procedure_number' => $object->getComplaint()->getPurchase()->getPurchaseNumber(),
            'procedure_name' => $object->getComplaint()->getPurchase()->getPurchaseName(),
            'type' => [
                'id' => $object->getStatus()->getId(),
                'value' => $object->getStatus()->getCode(),
                'title' => $object->getStatus()->getName()
            ],
            'control_body' => [
                'archive' => $object->getComplaint()->getControlOrganization()->isArchive(),
                'value' => $object->getComplaint()->getControlOrganization()->getId(),
                'title' => $object->getComplaint()->getControlOrganization()->getFullName() ?? $object->getComplaint()->getControlOrganization()->getName()
            ],
            'direction' => $object->getComplaint()->getDirection()->getCode(),
            'body' => $this->getComplaintDocument($object->getComplaint()),
            'body_html' => $this->getComplaintDocumentHtml($object->getComplaint()),
            'complaint_link' => $this->getComplaintLink($object),
            'reason_link' => $this->getDocumentLinks($object, Document::TYPE_DECISION),
            'prescription_link' => $this->getDocumentLinks($object, Document::TYPE_PRESCRIPTION),
            'registry_number' => $object->getComplaint()->getRegNumber(),
            'applicant_organisation' => [
                'inn' => $object->getComplaint()->getApplicantOrganization()?->getId(),
                'title' => $object->getComplaint()->getApplicantOrganization()?->getName()
            ],
            'control_subject_organisation' => [
                'inn' => $object->getControlSubjectOrganization()?->getInn(),
                'title' => $object->getControlSubjectOrganization()?->getName()
            ],
        ];
    }

    private function getComplaintDocument(Complaint $complaint): ?string
    {
        $text = $this->getComplaintDocumentHtml($complaint);
        return $text ? $this->complaintService->convertHtmlToText($text) : $text;
    }

    private function getComplaintDocumentHtml(Complaint $complaint): ?string
    {
        if ($complaint->getComplaintResult()->count() === 0) {
            return null;
        }

        $document = $complaint->getComplaintResult()->first()->getDocuments()
            ->filter(
                static function (Document $document) {
                    return $document->getType() === Document::TYPE_DECISION &&
                        $document->getStatus() === Document::STATUS_PROCESSED;
                }
            )->first();

        if (!$document || !$document->getContent()) {
            return null;
        }

        return $document->getContent();
    }

    private function getComplaintLink(ComplaintResult $object): string
    {
        return self::COMPLAINT_LINK . $object->getComplaint()->getExternalId();
    }

    private function getDocumentLinks(ComplaintResult $object, string $type): array
    {
        return $object->getDocuments()
            ->filter(static function (Document $document) use ($type) {
                return $document->getType() === $type;
            })
            ->map(static function (Document $document) {
                return $document->getUrl();
            })
            ->getValues();
    }
}
