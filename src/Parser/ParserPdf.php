<?php

namespace App\Parser;

use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Process\Process;

class ParserPdf implements ParserInterface
{
    public const TYPE_PDF = 'pdf';

    public function __construct(private ParameterBagInterface $parameterBag)
    {
    }

    /**
     * @throws Exception
     */
    public function parse(SplFileInfo $fileInfo): string
    {
        $process = new Process(['/usr/bin/pdftohtml', '-stdout', $fileInfo->getRealPath()]);

        try {
            $process->mustRun();
            $result = $process->getOutput();
        } catch (Exception $e) {
            throw $e;
        }

        $pos = stripos($result, "<!DOCTYPE HTML");
        if (!$result) {
            throw new Exception("Empty file content.");
        }
        if ($pos === false) {
            throw new Exception($result);
        }

        return $this->replaceImageUrls(substr($result, $pos, strlen($result)));
    }

    public function supports(string $extension): bool
    {
        return $extension === self::TYPE_PDF;
    }

    private function replaceImageUrls(string $string): string
    {
        if (!$this->parameterBag->get('document_source_dir')) {
            return $string;
        }
        return str_replace($this->parameterBag->get('document_source_dir'), "", $string);
    }
}