<?php

namespace App\Parser;

use Exception;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Process\Process;

class ParserUnoConv implements ParserInterface
{
    private const TYPE_AVAILABLE = [
        'xml',
        'xls',
        'odt',
        'doc',
        'docx',
        'htm',
        'txt',
        'html',
        'xlsx',
    ];

    /**
     * @throws Exception
     */
    public function parse(SplFileInfo $fileInfo): string
    {
        $process = new Process(['/usr/bin/unoconv', '--stdout', '-f', 'html', $fileInfo->getRealPath()]);

        try {
            $process->mustRun();
            $result = $process->getOutput();
        } catch (Exception $e) {
            throw $e;
        }

        $pos = stripos($result, "<!DOCTYPE HTML");
        if (!$result) {
            throw new Exception("Empty file content.");
        }
        if ($pos === false) {
            throw new Exception($result);
        }

        return substr($result, $pos, strlen($result));
    }

    public function supports(string $extension): bool
    {
        return in_array($extension, self::TYPE_AVAILABLE);
    }
}
