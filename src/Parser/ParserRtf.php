<?php

namespace App\Parser;

use LukeMadhanga\DocumentParser;
use Symfony\Component\Finder\SplFileInfo;

class ParserRtf implements ParserInterface
{
    public const TYPE_RTF = 'rtf';

    public function parse(SplFileInfo $fileInfo): string
    {
        $rtf = DocumentParser::parseFromFile($fileInfo->getRealPath(), 'application/rtf');

        return iconv('Windows-1251', 'UTF-8', $rtf);
    }

    public function supports(string $extension): bool
    {
        return $extension === self::TYPE_RTF;
    }
}