<?php

namespace App\Parser;

use Symfony\Component\Finder\SplFileInfo;

interface ParserInterface
{
    public function parse(SplFileInfo $fileInfo): string;

    public function supports(string $extension): bool;
}