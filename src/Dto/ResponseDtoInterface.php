<?php

namespace App\Dto;

interface ResponseDtoInterface extends DtoInterface
{
    public function toArray(): array;
}
