<?php

namespace App\Dto;

use Symfony\Component\HttpFoundation\Request;

interface ConstructRequestObjectInterface extends RequestDtoInterface
{
    public function __construct(Request $request);
}
