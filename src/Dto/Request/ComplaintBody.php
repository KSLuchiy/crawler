<?php

namespace App\Dto\Request;

use App\Dto\RequestObjectInterface;

class ComplaintBody implements RequestObjectInterface
{
    private int $id;
    private ?string $keywords = null;
    private ?bool $searchPhrase = null;

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'keywords' => $this->getKeywords(),
            'searchPhrase' => $this->getSearchPhrase(),
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): void
    {
        $this->keywords = $keywords;
    }

    public function getSearchPhrase(): ?bool
    {
        return $this->searchPhrase;
    }

    public function setSearchPhrase(?bool $searchPhrase): void
    {
        $this->searchPhrase = $searchPhrase;
    }
}