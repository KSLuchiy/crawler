<?php

namespace App\Dto\Request\FasItemFilter;

use App\Dto\RequestObjectInterface;

class SortingFilter implements RequestObjectInterface
{
    private ?string $field = null;
    private ?string $order = null;
    private ?bool $searchPhrase = null;
    private ?string $showType = null;

    public function toArray(): array
    {
        return [
            'field' => $this->getField(),
            'order' => $this->getOrder(),
            'search_phrase' =>  $this->getSearchPhrase(),
            'show_type' =>  $this->getShowType()
        ];
    }

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setField(?string $field): void
    {
        $this->field = $field;
    }

    public function getOrder(): ?string
    {
        return $this->order;
    }

    public function setOrder(?string $order): void
    {
        $this->order = $order;
    }

    public function getSearchPhrase(): ?bool
    {
        return $this->searchPhrase;
    }

    public function setSearchPhrase(?bool $searchPhrase): void
    {
        $this->searchPhrase = $searchPhrase;
    }

    public function getShowType(): ?string
    {
        return $this->showType;
    }

    public function setShowType(?string $showType): void
    {
        $this->showType = $showType;
    }
}
