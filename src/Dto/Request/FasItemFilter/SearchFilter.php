<?php

namespace App\Dto\Request\FasItemFilter;

use App\Dto\RequestObjectInterface;
use DateTime;

final class SearchFilter implements RequestObjectInterface
{
    private ?string $keywords = null;
    private ?int $controlBody = null;
    private ?string $direction = null;
    private ?string $type = null;
    private ?string $procedureNumber = null;
    private ?string $procedureName = null;
    private ?DateTime $reasonDateFrom = null;
    private ?DateTime $reasonDateTo = null;
    private ?DateTime $createDateFrom = null;
    private ?DateTime $createDateTo = null;
    private ?string $registryNumber = null;
    private ?string $applicantOrganisation = null;
    private ?string $controlSubjectOrganisation = null;

    public function toArray(): array
    {
        return [
            "keywords" => $this->keywords,
            "control_body.value" => $this->controlBody,
            "direction" => $this->direction,
            "type.value" => $this->type,
            "procedure_number" => $this->procedureNumber,
            "procedure_name" => $this->procedureName,
            "reason_date_from" => $this->reasonDateFrom,
            "reason_date_to" => $this->reasonDateTo,
            "create_date_from" => $this->createDateFrom,
            "create_date_to" => $this->createDateTo,
            "registry_number" => $this->registryNumber,
            "applicant_organisation" => $this->applicantOrganisation,
            "control_subject_organisation" => $this->controlSubjectOrganisation,
        ];
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): SearchFilter
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function getControlBody(): ?int
    {
        return $this->controlBody;
    }

    public function setControlBody(?int $controlBody): SearchFilter
    {
        $this->controlBody = $controlBody;
        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(?string $direction): void
    {
        $this->direction = $direction;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getProcedureNumber(): ?string
    {
        return $this->procedureNumber;
    }

    public function setProcedureNumber(?string $procedureNumber): void
    {
        $this->procedureNumber = $procedureNumber;
    }

    public function getProcedureName(): ?string
    {
        return $this->procedureName;
    }

    public function setProcedureName(?string $procedureName): void
    {
        $this->procedureName = $procedureName;
    }

    public function getReasonDateFrom(): ?DateTime
    {
        return $this->reasonDateFrom;
    }

    public function setReasonDateFrom(?DateTime $reasonDateFrom): void
    {
        $this->reasonDateFrom = $reasonDateFrom;
    }

    public function getReasonDateTo(): ?DateTime
    {
        return $this->reasonDateTo;
    }

    public function setReasonDateTo(?DateTime $reasonDateTo): void
    {
        $this->reasonDateTo = $reasonDateTo;
    }

    public function getCreateDateFrom(): ?DateTime
    {
        return $this->createDateFrom;
    }

    public function setCreateDateFrom(?DateTime $createDateFrom): void
    {
        $this->createDateFrom = $createDateFrom;
    }

    public function getCreateDateTo(): ?DateTime
    {
        return $this->createDateTo;
    }

    public function setCreateDateTo(?DateTime $createDateTo): void
    {
        $this->createDateTo = $createDateTo;
    }

    public function getRegistryNumber(): ?string
    {
        return $this->registryNumber;
    }

    public function setRegistryNumber(?string $registryNumber): void
    {
        $this->registryNumber = $registryNumber;
    }

    public function getApplicantOrganisation(): ?string
    {
        return $this->applicantOrganisation;
    }

    public function setApplicantOrganisation(?string $applicantOrganisation): void
    {
        $this->applicantOrganisation = $applicantOrganisation;
    }

    public function getControlSubjectOrganisation(): ?string
    {
        return $this->controlSubjectOrganisation;
    }

    public function setControlSubjectOrganisation(?string $controlSubjectOrganisation): void
    {
        $this->controlSubjectOrganisation = $controlSubjectOrganisation;
    }
}
