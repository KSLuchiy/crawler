<?php

namespace App\Dto\Request;

use App\Dto\Request\FasItemFilter\SearchFilter;
use App\Dto\Request\FasItemFilter\SortingFilter;
use App\Dto\RequestObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

class FasItemFilter implements RequestObjectInterface
{
    private int $offset = 0;

    #[Assert\LessThan(100)]
    private int $limit = 10;
    private ?SearchFilter $searchForm = null;
    private ?SortingFilter $order = null;

    public function toArray(): array
    {
        return [
            'offset' => $this->offset,
            'limit' => $this->limit,
            'order' => $this->order?->toArray() ?? [],
            'search_form' => $this->searchForm?->toArray() ?? []
        ];
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function getSearchForm(): ?SearchFilter
    {
        return $this->searchForm;
    }

    public function setSearchForm(?SearchFilter $searchForm): void
    {
        $this->searchForm = $searchForm;
    }

    public function getOrder(): ?SortingFilter
    {
        return $this->order;
    }

    public function setOrder(?SortingFilter $order): void
    {
        $this->order = $order;
    }
}