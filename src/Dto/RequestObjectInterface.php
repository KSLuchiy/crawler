<?php

namespace App\Dto;

interface RequestObjectInterface extends RequestDtoInterface
{
    public function toArray(): array;
}
