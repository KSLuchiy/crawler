<?php

namespace App\Dto\Response;

use App\Dto\ResponseDtoInterface;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation as Annotation;

final class Complaint implements ResponseDtoInterface
{
    #[Assert\NotBlank]
    public int $id;

    #[Annotation\SerializedName('created_at')]
    #[Assert\Type(DateTime::class)]
    #[Assert\NotBlank]
    public DateTime $createdAt;

    #[Annotation\SerializedName('updated_at')]
    #[Assert\Type(DateTime::class)]
    #[Assert\NotBlank]
    public DateTime $updatedAt;

    #[Annotation\SerializedName('reasoned_at')]
    #[Assert\Type(DateTime::class)]
    #[Assert\NotBlank]
    public DateTime $reasonedAt;

    #[Annotation\SerializedName('procedure_number')]
    #[Assert\NotBlank]
    public string $procedureNumber;

    #[Annotation\SerializedName('procedure_name')]
    #[Assert\NotBlank]
    public string $procedureName;

    #[Assert\NotBlank]
    public FasType $type;

    #[Annotation\SerializedName('control_body')]
    #[Assert\NotBlank]
    public ControlBody $controlBody;

    #[Assert\NotBlank]
    public string $direction;

    public ?string $body = null;
    public ?string $preview = null;

    #[Annotation\SerializedName('complaint_link')]
    public ?string $complaintLink;

    #[Annotation\SerializedName('reason_link')]
    public array $reasonLink;

    #[Annotation\SerializedName('prescription_link')]
    public array $prescriptionLink;

    #[Annotation\SerializedName('registry_number')]
    public ?string $registryNumber;

    #[Annotation\SerializedName('applicant_organisation')]
    public array $applicantOrganisation;

    #[Annotation\SerializedName('control_subject_organisation')]
    public array $controlSubjectOrganisation;

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'createdAt' => $this->createdAt->format('Y-m-d H:i:s.u'),
            'updatedAt' => $this->updatedAt->format('Y-m-d H:i:s.u'),
            'reasonedAt' => $this->reasonedAt->format('Y-m-d H:i:s.u'),
            'procedureNumber' => $this->procedureNumber,
            'procedureName' => $this->procedureName,
            'type' => $this->type->toArray(),
            'controlBody' => $this->controlBody->toArray(),
            'direction' => $this->direction,
            'body' => $this->body,
            'preview' => $this->preview,
            'complaintLink' => $this->complaintLink,
            'reasonLink' => $this->reasonLink,
            'prescriptionLink' => $this->prescriptionLink,
            'registryNumber' => $this->registryNumber,
            'applicantOrganisation' => $this->applicantOrganisation,
            'controlSubjectOrganisation' => $this->controlSubjectOrganisation,
        ];
    }

    public function getType(): FasType
    {
        return $this->type;
    }
}
