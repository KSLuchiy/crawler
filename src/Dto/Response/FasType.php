<?php

namespace App\Dto\Response;

use App\Dto\ResponseDtoInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class FasType implements ResponseDtoInterface
{
    #[Assert\NotBlank]
    public string $title;

    #[Assert\NotBlank]
    public string $value;

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'value' => $this->value
        ];
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
