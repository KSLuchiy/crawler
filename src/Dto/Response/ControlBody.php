<?php

namespace App\Dto\Response;

use App\Dto\ResponseDtoInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class ControlBody implements ResponseDtoInterface
{
    #[Assert\NotBlank]
    public string $title;

    #[Assert\NotBlank]
    public string $value;

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'value' => $this->value
        ];
    }
}
