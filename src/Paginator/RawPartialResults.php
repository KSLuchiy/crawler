<?php

namespace App\Paginator;

use Elastica\Result;
use FOS\ElasticaBundle\Paginator\RawPartialResults as BaseRawPartialResults;

class RawPartialResults extends BaseRawPartialResults
{
    public function toArray(): array
    {
        return \array_map(static function (Result $result) {
            return [
                'source' => $result->getSource(),
                'highlight' => $result->getHighlights()
            ];
        }, $this->resultSet->getResults());
    }
}
