<?php

namespace App\Paginator;

use FOS\ElasticaBundle\Paginator\RawPaginatorAdapter as BaseRawPaginatorAdapter;

class RawPaginatorAdapter extends BaseRawPaginatorAdapter
{
    public function getResults($offset, $itemCountPerPage)
    {
        return new RawPartialResults($this->getElasticaResults($offset, $itemCountPerPage));
    }
}
