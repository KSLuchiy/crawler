<?php

namespace App\Normalizer;

use App\Exception\DtoValidationException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolation;

class DtoExceptionNormalizer implements NormalizerInterface
{
    /**
     * @param mixed $object
     * @param string|null $format
     * @param array $context
     * @return array
     */
    public function normalize($object, ?string $format = null, array $context = []): array
    {
        $data = [];
        $errors = $object->getErrors();

        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $data[$error->getPropertyPath()] = $error->getMessage();
        }

        return $data;
    }

    /**
     * @param mixed $data
     * @param string|null $format
     * @return bool
     */
    public function supportsNormalization($data, ?string $format = null): bool
    {
        return $data instanceof DtoValidationException;
    }
}
