<?php

namespace App\Process;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

class ProcessPool
{
    /**
     * @var  LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var array
     */
    private array $pool = [];

    /**
     * @var int
     */
    private int $maxProcesses = 10;

    /**
     * @var int
     */
    private int $intervalMicroSeconds = 5000;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Process $process
     */
    public function append(Process $process): void
    {
        $this->pool[] = $process;
    }

    /**
     * @return int
     */
    private function getActiveProcessCount(): int
    {
        return count(array_filter($this->pool, function ($process) {
            /** @var Process $process */
            return $process->isRunning();
        }));
    }

    /**
     * @throws Exception
     */
    public function start(): void
    {
        $this->logger->debug('Starting queue consumption.');

        while (count($this->pool) > 0) {
            /** @var Process $process */
            foreach ($this->pool as $key => $process) {
                usleep($this->intervalMicroSeconds);

                if ($process->isRunning()) {
                    continue;
                }

                if ($process->isTerminated()) {
                    if (!$process->isSuccessful()) {
                        $this->logger->error('Sub process failed.', [
                            'command' => $process->getCommandLine(),
                            'output' => $process->getOutput()
                        ]);
                    }

                    unset($this->pool[$key]);
                    continue;
                }

                if ($this->getActiveProcessCount() < $this->maxProcesses) {
                    $process->start();
                }
            }
        }

        $this->logger->debug('All processes are done.');
    }
}