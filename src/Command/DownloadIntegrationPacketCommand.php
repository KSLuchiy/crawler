<?php

namespace App\Command;

use App\Service\IntegrationPacketService;
use App\Trait\WriteLogTrait;
use DateTime;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:download-integration-packet',
    description: 'Task download packet from ftp',
)]
class DownloadIntegrationPacketCommand extends Command
{
    use WriteLogTrait;

    public function __construct(private IntegrationPacketService $integrationPacketService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('source', 's', InputOption::VALUE_REQUIRED, 'The filename in the remote source.')
            ->addOption('min-date', 'md', InputOption::VALUE_OPTIONAL, 'Min modify date');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->write($output, 'Start download integration packet.');

        if (empty($input->getOption('source'))) {
            $this->write($output, 'Download integration packet failed.');

            return Command::FAILURE;
        }

        try {
            $this->integrationPacketService->download(
                $input->getOption('source'),
                !empty($input->getOption('min-date'))
                    ? new DateTime((string)$input->getOption('min-date'))
                    : null
            );

            $this->write($output, 'Download integration packet success.');
        } catch (Throwable $exception) {
            $this->write($output, $exception->getMessage());

            return Command::FAILURE;
        }

        $this->write($output, 'Finish download integration packet.');

        return Command::SUCCESS;
    }
}
