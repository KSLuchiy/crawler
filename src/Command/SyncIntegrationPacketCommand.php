<?php

namespace App\Command;

use App\Service\IntegrationPacketService;
use App\Trait\WriteLogTrait;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:sync-integration-packet',
    description: 'Synchronize integration packets from ftp',
)]
class SyncIntegrationPacketCommand extends Command
{
    use WriteLogTrait;

    public function __construct(private IntegrationPacketService $integrationPacketService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('source', 's', InputOption::VALUE_REQUIRED, 'The path in the remote source')
            ->addOption('min-date', 'md', InputOption::VALUE_OPTIONAL, 'Min modify date');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {   $this->write($output, 'Start sync integration packets.');

        if (empty($input->getOption('source'))) {
            $this->write($output, 'Sync integration packets failed.');

            return Command::FAILURE;
        }

        try {
            $this->integrationPacketService->sync(
                $input->getOption('source'),
                !empty($input->getOption('min-date')) ? $input->getOption('min-date') : null
            );

            $this->write($output, 'Sync integration packets success.');
        } catch (\Throwable $exception) {
            $this->write($output, $exception->getMessage());

            return Command::FAILURE;
        }

        $this->write($output, 'Finish sync integration packets.');

        return Command::SUCCESS;
    }
}
