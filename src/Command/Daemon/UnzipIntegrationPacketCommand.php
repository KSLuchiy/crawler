<?php

namespace App\Command\Daemon;

use App\Entity\IntegrationPacket;
use App\Entity\IntegrationPacket\PacketZip;
use App\Repository\IntegrationPacketRepository;
use App\Service\IntegrationPacketService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    name: 'app:unzip-integration-packet',
    description: 'Daemon on unzip packets',
)]
class UnzipIntegrationPacketCommand extends AbstractIntegrationPacketCommand
{
    public const DEFAULT_TIMEOUT = 0.1;

    protected string $discriminator = PacketZip::DISCRIMINATOR;
    protected string $class = PacketZip::class;

    /**
     * @throws Exception
     */
    protected function process(IntegrationPacket $packet): bool
    {
        if (!$packet instanceof PacketZip) {
            return false;
        }
        return $this->integrationPacketService->unzip($packet);
    }
}
