<?php

namespace App\Command\Daemon;

use App\Trait\WriteLogTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Wrep\Daemonizable\Command\EndlessContainerAwareCommand;

abstract class AbstractEndlessCommand extends EndlessContainerAwareCommand
{
    use WriteLogTrait;

    protected const EMPTY_QUERY_TIMEOUT = 60;
    private int $iterationCount = 0;

    public function __construct()
    {
        parent::__construct();

        $this->addOption('max-iteration', null, InputOption::VALUE_OPTIONAL, 'Max iteration');
    }

    protected function startIteration(InputInterface $input, OutputInterface $output): void
    {
        parent::startIteration($input, $output);
    }

    protected function finishIteration(InputInterface $input, OutputInterface $output): void
    {
        $this->iterationCount++;

        parent::finishIteration($input, $output);

        gc_collect_cycles();

        if ($input->getOption('max-iteration') &&
            (int)$input->getOption('max-iteration') <= $this->iterationCount
        ) {
            $this->shutdown();
        }
    }
}
