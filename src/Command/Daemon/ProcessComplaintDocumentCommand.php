<?php

namespace App\Command\Daemon;

use App\Entity\Document;
use App\Repository\DocumentRepository;
use App\Service\DocumentService;
use App\Service\LockerService;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(
    name: 'app:process-complaint-document',
    description: 'Daemon download and parse complaint document',
)]
class ProcessComplaintDocumentCommand extends AbstractEndlessCommand
{
    public const DEFAULT_TIMEOUT = 0.0;

    public function __construct(
        private DocumentRepository $documentRepository,
        private DocumentService $documentService,
        private LockerService $locker
    ) {
        parent::__construct();
    }

    /**
     * @throws NonUniqueResultException
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $document = $this->documentRepository->findOneReadyDocument(
            Document::STATUS_READY,
            $this->locker->getLookedIds(static::class)
        );

        if (!$document instanceof Document) {
            $this->write($output, 'Document not found');
            sleep(self::EMPTY_QUERY_TIMEOUT);

            return;
        }

        $this->write($output, sprintf('Document found: %d', $document->getId()));

        if ($this->locker->isLocked(static::class, $document->getId())) {
            return;
        }

        try {
            $this->locker->lock(static::class, $document->getId());
            $this->documentService->processFindComplaintBody($document);
            $this->locker->unlock(static::class, $document->getId());
        } catch (Throwable $exception) {
            $this->locker->unlock(static::class, $document->getId());

            $this->write($output, $exception->getMessage());
        }
    }
}
