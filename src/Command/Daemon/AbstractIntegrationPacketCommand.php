<?php

namespace App\Command\Daemon;

use App\Entity\IntegrationPacket;
use App\Exception\PacketHandlerException;
use App\Exception\PacketValidationException;
use App\Repository\IntegrationPacketRepository;
use App\Service\IntegrationPacketService;
use App\Service\LockerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

abstract class AbstractIntegrationPacketCommand extends AbstractEndlessCommand
{
    protected string $discriminator = IntegrationPacket::class;
    protected string $class = IntegrationPacket::class;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected IntegrationPacketRepository $integrationPacketRepository,
        protected IntegrationPacketService $integrationPacketService,
        protected LockerService $locker
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    /**
     * @throws NonUniqueResultException
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $packet = $this->getPacketForProcess($this->locker->getLookedIds(static::class));
        if (!$packet instanceof $this->class) {
            $this->write($output, sprintf('%s packet not found found', $this->discriminator));
            sleep(self::EMPTY_QUERY_TIMEOUT);

            return;
        }

        $this->write($output, sprintf('%s packet found: %d', $this->discriminator, $packet->getId()));

        if ($this->locker->isLocked(static::class, $packet->getId())) {
            return;
        }

        try {
            $this->locker->lock(static::class, $packet->getId());
            $this->integrationPacketService->lockPacket($packet);

            $packet
                ->setStatus(
                    $this->process($packet)
                        ? IntegrationPacket::STATUS_SUCCESS_PROCESSED
                        : IntegrationPacket::STATUS_ERROR_PROCESS
                )
                ->setLogMessage(null);

            $this->write($output, sprintf('%s packet processed: %d', $this->discriminator, $packet->getId()));
            $this->locker->unlock(static::class, $packet->getId());
        } catch (Throwable $exception) {
            if ($exception instanceof PacketHandlerException) {
                $packet->setStatus(IntegrationPacket::STATUS_ERROR_PROCESS_HANDLER);
            } elseif ($exception instanceof PacketValidationException) {
                $packet->setStatus(IntegrationPacket::STATUS_ERROR_VALIDATION);
            } else {
                $packet->setStatus(IntegrationPacket::STATUS_ERROR_PROCESS);
            }

            $packet->setLogMessage($exception->getMessage());

            $this->write($output, $exception->getMessage());
            $this->locker->unlock(static::class, $packet->getId());
        }

        $this->entityManager->persist($packet);
        $this->entityManager->flush();
    }

    abstract protected function process(IntegrationPacket $packet): bool;

    /**
     * @throws NonUniqueResultException
     */
    protected function getPacketForProcess(array $ids = []): ?IntegrationPacket
    {
        return $this->integrationPacketRepository->findOneByStatus(
            IntegrationPacket::STATUS_SUCCESS,
            $this->discriminator,
            $ids
        );
    }
}
