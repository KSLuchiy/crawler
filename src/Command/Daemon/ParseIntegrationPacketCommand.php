<?php

namespace App\Command\Daemon;

use App\Entity\IntegrationPacket;
use App\Entity\IntegrationPacket\PacketXml;
use App\Handler\PacketHandlerInterface;
use App\Repository\IntegrationPacketRepository;
use App\Service\IntegrationPacketService;
use App\Service\LockerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Throwable;

#[AsCommand(
    name: 'app:parse-integration-packet',
    description: 'Daemon parse xml file packet',
)]
class ParseIntegrationPacketCommand extends AbstractIntegrationPacketCommand
{
    public const DEFAULT_TIMEOUT = 0.0;

    protected string $discriminator = PacketXml::DISCRIMINATOR;
    protected string $class = PacketXml::class;

    public function __construct(
        EntityManagerInterface $entityManager,
        IntegrationPacketRepository $integrationPacketRepository,
        IntegrationPacketService $integrationPacketService,
        LockerService $locker,
        private PacketHandlerInterface $packetHandler,
    ) {
        parent::__construct($entityManager, $integrationPacketRepository, $integrationPacketService, $locker);
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    protected function process(IntegrationPacket $packet): bool
    {
        if (!$packet instanceof PacketXml) {
            return false;
        }
        return $this->packetHandler->handle($packet);
    }

    /**
     * @throws NonUniqueResultException
     */
    protected function getPacketForProcess(array $ids = []): ?IntegrationPacket
    {
        return $this->integrationPacketRepository->findOneByStatusAndPriority(
            IntegrationPacket::STATUS_SUCCESS,
            $this->discriminator,
            $ids
        );
    }
}
