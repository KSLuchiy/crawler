<?php

namespace App\Entity\Organization;

use App\Entity\Organization;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class IndividualPerson extends Organization
{
    public const DISCRIMINATOR = 'individual-person';
}
