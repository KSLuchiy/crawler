<?php

namespace App\Entity;

use App\Repository\ControlOrganizationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[
    ORM\Entity(repositoryClass: ControlOrganizationRepository::class),
    ORM\Table(name: 'control_organization'),
]
class ControlOrganization
{
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(type: Types::STRING, length: 16)]
    private string $regNum;

    #[ORM\Column(type: Types::STRING, length: 16)]
    private ?string $consRegistryNum = null;

    #[ORM\Column(type: Types::STRING, length: 2000)]
    private string $name;

    #[ORM\Column(type: Types::STRING, length: 2000, nullable: true)]
    private ?string $fullName = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: true, options: ['default' => false])]
    private bool $archive = false;

    #[ORM\Column(type: Types::STRING, length: 16)]
    private ?string $inn = null;

    #[ORM\Column(type: Types::STRING, length: 16)]
    private ?string $kpp = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getRegNum(): string
    {
        return $this->regNum;
    }

    public function setRegNum(string $regNum): self
    {
        $this->regNum = $regNum;
        return $this;
    }

    public function getConsRegistryNum(): ?string
    {
        return $this->consRegistryNum;
    }

    public function setConsRegistryNum(?string $consRegistryNum): self
    {
        $this->consRegistryNum = $consRegistryNum;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function setInn(?string $inn): self
    {
        $this->inn = $inn;
        return $this;
    }

    public function getKpp(): ?string
    {
        return $this->kpp;
    }

    public function setKpp(?string $kpp): self
    {
        $this->kpp = $kpp;
        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;
        return $this;
    }

    public function isArchive(): bool
    {
        return $this->archive;
    }

    public function setArchive(bool $archive): self
    {
        $this->archive = $archive;
        return $this;
    }
}
