<?php

namespace App\Entity;

use App\Entity\Document\ComplaintResultDocument;
use App\Repository\ComplaintResultRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ComplaintResultRepository::class)]
class ComplaintResult
{
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(name: 'created_at', type: Types::DATETIME_MUTABLE)]
    private DateTime $createdAt;

    #[ORM\Column(name: 'reasoned_at', type: Types::DATETIME_MUTABLE)]
    private DateTime $reasonedAt;

    #[ORM\ManyToOne(targetEntity: ComplaintStatus::class, cascade: ["persist"])]
    #[ORM\JoinColumn(name: 'status_id', referencedColumnName: 'id', nullable: true)]
    private ?ComplaintStatus $status = null;

    #[ORM\ManyToOne(targetEntity: Complaint::class, cascade: ["persist"])]
    #[ORM\JoinColumn(name: 'complaint_id', referencedColumnName: 'id')]
    private Complaint $complaint;

    #[ORM\ManyToOne(targetEntity: ControlOrganization::class, cascade: ["persist"])]
    #[ORM\JoinColumn(name: 'control_subject_organization_id', referencedColumnName: 'id')]
    private ?ControlOrganization $controlSubjectOrganization = null;

    #[ORM\OneToMany(mappedBy: 'complaintResult', targetEntity: ComplaintResultDocument::class, cascade: ["persist"])]
    private Collection $documents;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function getComplaint(): Complaint
    {
        return $this->complaint;
    }

    public function setComplaint(Complaint $complaint): self
    {
        $this->complaint = $complaint;
        return $this;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getReasonedAt(): DateTime
    {
        return $this->reasonedAt;
    }

    public function setReasonedAt(DateTime $reasonedAt): self
    {
        $this->reasonedAt = $reasonedAt;
        return $this;
    }

    public function getStatus(): ?ComplaintStatus
    {
        return $this->status;
    }

    public function setStatus(?ComplaintStatus $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getDocuments(): ArrayCollection|Collection
    {
        return $this->documents;
    }

    public function setDocuments(ArrayCollection|Collection $documents): self
    {
        $this->documents = $documents;
        return $this;
    }

    public function addDocument(ComplaintResultDocument $document): self
    {
        $this->documents->add($document);
        return $this;
    }

    public function getControlSubjectOrganization(): ?ControlOrganization
    {
        return $this->controlSubjectOrganization;
    }

    public function setControlSubjectOrganization(?ControlOrganization $controlSubjectOrganization): self
    {
        $this->controlSubjectOrganization = $controlSubjectOrganization;
        return $this;
    }
}
