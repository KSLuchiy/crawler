<?php

namespace App\Entity;

use App\Entity\Document\ComplaintDocument;
use App\Repository\ComplaintRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ComplaintRepository::class)]
class Complaint
{
    public const DEFAULT_VERSION = 1;

    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(name: 'created_at', type: Types::DATETIME_MUTABLE)]
    private DateTime $createdAt;

    #[ORM\Column(name: 'updated_at', type: Types::DATETIME_MUTABLE)]
    private DateTime $updatedAt;

    #[ORM\Column(name: 'reg_number', type: Types::STRING, length: 32, nullable: true)]
    private ?string $regNumber = null;

    #[ORM\ManyToOne(targetEntity: Purchase::class, cascade: ["persist"])]
    #[ORM\JoinColumn(name: 'purchase_id', referencedColumnName: 'id')]
    private Purchase $purchase;

    #[ORM\ManyToOne(targetEntity: ControlOrganization::class, cascade: ["persist"])]
    #[ORM\JoinColumn(name: 'control_organization_id', referencedColumnName: 'id')]
    private ControlOrganization $controlOrganization;

    #[ORM\ManyToOne(targetEntity: Organization::class, cascade: ["persist"])]
    #[ORM\JoinColumn(name: 'applicant_organization_id', referencedColumnName: 'id')]
    private ?Organization $applicantOrganization = null;

    #[ORM\ManyToOne(targetEntity: Direction::class, cascade: ["persist"])]
    #[ORM\JoinColumn(name: 'direction_id', referencedColumnName: 'id')]
    private Direction $direction;

    #[ORM\OneToOne(targetEntity: RegType::class)]
    #[ORM\JoinColumn(name: 'reg_type_id', referencedColumnName: 'id')]
    private RegType $regType;

    #[ORM\Column(name: 'external_id', type: Types::INTEGER)]
    private int $externalId;

    #[ORM\Column(name: 'version', type: Types::INTEGER)]
    private int $version = self::DEFAULT_VERSION;

    #[ORM\OneToMany(mappedBy: 'complaint', targetEntity: ComplaintResult::class, cascade: ["persist"])]
    private Collection $complaintResult;

    #[ORM\OneToMany(mappedBy: 'complaint', targetEntity: ComplaintDocument::class, cascade: ["persist"])]
    private Collection $documents;

    #[ORM\Column(name: 'complaint_number', type: Types::STRING, length: 255, nullable: true)]
    private ?string $complaintNumber = null;

    public function __construct()
    {
        $this->complaintResult = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getRegNumber(): ?string
    {
        return $this->regNumber;
    }

    public function setRegNumber(?string $regNumber): self
    {
        $this->regNumber = $regNumber;
        return $this;
    }

    public function getPurchase(): Purchase
    {
        return $this->purchase;
    }

    public function setPurchase(Purchase $purchase): self
    {
        $this->purchase = $purchase;
        return $this;
    }

    public function getControlOrganization(): ControlOrganization
    {
        return $this->controlOrganization;
    }

    public function setControlOrganization(ControlOrganization $controlOrganization): self
    {
        $this->controlOrganization = $controlOrganization;
        return $this;
    }

    public function getApplicantOrganization(): ?Organization
    {
        return $this->applicantOrganization;
    }

    public function setApplicantOrganization(?Organization $applicantOrganization): self
    {
        $this->applicantOrganization = $applicantOrganization;
        return $this;
    }

    public function getDirection(): Direction
    {
        return $this->direction;
    }

    public function setDirection(Direction $direction): self
    {
        $this->direction = $direction;
        return $this;
    }

    public function getDocuments(): ArrayCollection|Collection
    {
        return $this->documents;
    }

    public function setDocuments(ArrayCollection|Collection $documents): self
    {
        $this->documents = $documents;
        return $this;
    }

    public function addDocument(ComplaintDocument $document): self
    {
        $this->documents->add($document);
        return $this;
    }

    public function getComplaintResult(): ArrayCollection|Collection
    {
        return $this->complaintResult;
    }

    public function addComplaintResult(ComplaintResult $complaintResult): self
    {
        $this->complaintResult->add($complaintResult);
        return $this;
    }

    public function getExternalId(): int
    {
        return $this->externalId;
    }

    public function setExternalId(int $externalId): self
    {
        $this->externalId = $externalId;
        return $this;
    }

    public function getRegType(): RegType
    {
        return $this->regType;
    }

    public function setRegType(RegType $regType): self
    {
        $this->regType = $regType;
        return $this;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): self
    {
        $this->version = $version;
        return $this;
    }

    public function getComplaintNumber(): ?string
    {
        return $this->complaintNumber;
    }

    public function setComplaintNumber(?string $complaintNumber): self
    {
        $this->complaintNumber = $complaintNumber;

        return $this;
    }
}
