<?php

namespace App\Entity;

use App\Entity\IntegrationPacket\PacketXml;
use App\Entity\IntegrationPacket\PacketZip;
use App\Repository\IntegrationPacketRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[
    ORM\Entity(repositoryClass: IntegrationPacketRepository::class),
    ORM\Table(name: 'integration_packet'),
    ORM\InheritanceType('SINGLE_TABLE'),
    ORM\DiscriminatorColumn(name: 'type', type: 'string'),
    ORM\DiscriminatorMap(['zip' => PacketZip::class, 'xml' => PacketXml::class])
]
class IntegrationPacket implements FileInterface
{
    public const MAX_VALUE_PRIORITY = 999;

    public const STATUS_READY = 0; // Готов к загрузке
    public const STATUS_ERROR = -1; // Ошибка при загрузке
    public const STATUS_ERROR_UNPACKED = -2; // Ошибка распаковки
    public const STATUS_ERROR_PROCESS = -3; // Ошибка обработки общая
    public const STATUS_ERROR_VALIDATION = -4; // Ошибка обработки валидация
    public const STATUS_ERROR_PROCESS_HANDLER = -5; // Ошибка обработки handler
    public const STATUS_SUCCESS = 1; // Готов к распаковки
    public const STATUS_PROCESS = 2; // В обработке
    public const STATUS_UNPACKED = 3; // Готов к обработке
    public const STATUS_SUCCESS_PROCESSED = 4; // Последний статус - обработан

    #[
        ORM\Column(type: Types::INTEGER),
        ORM\Id,
        ORM\GeneratedValue(strategy: 'AUTO')
    ]
    private int $id;

    #[ORM\Column(type: Types::STRING, length: 36)]
    private string $guid;

    #[
        ORM\ManyToOne(targetEntity: IntegrationPacket::class, inversedBy: 'children'),
        ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', nullable: true),
    ]
    private ?IntegrationPacket $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: IntegrationPacket::class)]
    private Selectable $children;

    #[ORM\Column(type: Types::STRING, length: 50)]
    private string $name;

    #[ORM\Column(type: Types::STRING, length: 150)]
    private string $path;

    #[ORM\Column(type: Types::INTEGER, length: 100)]
    private int $size;

    #[ORM\Column(name: 'modify_date', type: Types::DATETIME_MUTABLE)]
    private DateTime $modifyDate;

    #[ORM\Column(type: Types::STRING, length: 100, nullable: true)]
    private string $hash;

    #[ORM\Column(type: Types::SMALLINT)]
    private int $status;

    #[ORM\Column(type: Types::SMALLINT)]
    private int $priority = self::MAX_VALUE_PRIORITY;

    #[ORM\Column(type: Types::STRING, length: 32, nullable: true)]
    private ?string $version = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $logMessage = null;

    protected static string $dirName = 'packet';

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    public function getParent(): ?IntegrationPacket
    {
        return $this->parent;
    }

    public function setParent(?IntegrationPacket $parent): self
    {
        $this->parent = $parent;
        return $this;
    }

    public function getChildren(): ArrayCollection
    {
        return $this->children;
    }

    public function setChildren(ArrayCollection $children): self
    {
        $this->children = $children;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;
        return $this;
    }

    public function getModifyDate(): DateTime
    {
        return $this->modifyDate;
    }

    public function setModifyDate(DateTime $modifyDate): self
    {
        $this->modifyDate = $modifyDate;
        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getDirName(): string
    {
        return DIRECTORY_SEPARATOR . self::$dirName . DIRECTORY_SEPARATOR . static::$dirName . DIRECTORY_SEPARATOR;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;
        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;
        return $this;
    }

    public function getLogMessage(): ?string
    {
        return $this->logMessage;
    }

    public function setLogMessage(?string $logMessage): self
    {
        $this->logMessage = $logMessage;
        return $this;
    }
}
