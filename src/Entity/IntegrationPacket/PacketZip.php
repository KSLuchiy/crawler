<?php

namespace App\Entity\IntegrationPacket;

use App\Entity\IntegrationPacket;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class PacketZip extends IntegrationPacket
{
    public const DISCRIMINATOR = 'zip';

    protected static string $dirName = 'zip';
}
