<?php

namespace App\Entity\IntegrationPacket;

use App\Entity\IntegrationPacket;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class PacketXml extends IntegrationPacket
{
    public const DISCRIMINATOR = 'xml';

    protected static string $dirName = 'xml';
}
