<?php

namespace App\Entity;

use App\Entity\Organization\ControlSubject;
use App\Entity\Organization\IndividualBusinessman;
use App\Entity\Organization\IndividualPerson;
use App\Entity\Organization\LegalEntity;
use App\Repository\OrganizationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[
    ORM\Entity(repositoryClass: OrganizationRepository::class),
    ORM\Table(name: 'organization'),
    ORM\InheritanceType('SINGLE_TABLE'),
    ORM\DiscriminatorColumn(name: 'discriminator', type: 'string'),
    ORM\DiscriminatorMap([
        'legal-entity' => LegalEntity::class,
        'control-subject' => ControlSubject::class,
        'individual-person' => IndividualPerson::class,
        'individual-businessman' => IndividualBusinessman::class,
    ])
]
class Organization
{
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(type: Types::STRING, length: 2000)]
    private string $name;

    #[ORM\Column(type: Types::STRING, length: 16)]
    private ?string $inn = null;

    #[ORM\Column(type: Types::STRING, length: 16)]
    private ?string $kpp = null;

    #[ORM\Column(type: Types::STRING, length: 16)]
    private ?string $regNum = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function setInn(?string $inn): self
    {
        $this->inn = $inn;
        return $this;
    }

    public function getKpp(): ?string
    {
        return $this->kpp;
    }

    public function setKpp(?string $kpp): self
    {
        $this->kpp = $kpp;
        return $this;
    }

    public function getRegNum(): ?string
    {
        return $this->regNum;
    }

    public function setRegNum(?string $regNum): self
    {
        $this->regNum = $regNum;
        return $this;
    }
}
