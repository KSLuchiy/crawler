<?php

namespace App\Entity;

interface FileInterface
{
    /**
     * @return string
     */
    public function getDirName(): string;
}