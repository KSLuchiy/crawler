<?php

namespace App\Entity;

use App\Entity\Document\ComplaintDocument;
use App\Entity\Document\ComplaintResultDocument;
use App\Repository\DocumentRepository;
use DateTime;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[
    ORM\Entity(repositoryClass: DocumentRepository::class),
    ORM\Table(name: 'document'),
    ORM\InheritanceType('SINGLE_TABLE'),
    ORM\DiscriminatorColumn(name: 'discriminator', type: 'string'),
    ORM\DiscriminatorMap([
        'complaint' => ComplaintDocument::class,
        'complaint-result' => ComplaintResultDocument::class
    ])
]
class Document implements FileInterface
{
    /**
     * Статус обработки документов
     * @var int
     */
    public const STATUS_READY = 0; // Готов к загрузке
    public const STATUS_ERROR = -1; // Ошибка при загрузке
    public const STATUS_ERROR_PROCESSED = -2; // Ошибка при обработке
    public const STATUS_SUCCESS = 1; // Готов к обработке
    public const STATUS_WAIT_CONFIRM = 3; // Ожидание подтверждения для загрузки
    public const STATUS_PROCESSED = 2; // Последний статут - Обработан

    /**
     * Тип документа
     * @var string
     */
    public const TYPE_DECISION = 'decision';
    public const TYPE_PRESCRIPTION = 'prescription';

    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(type: Types::STRING, length: 256)]
    private string $name;

    #[ORM\Column(name: 'real_name', type: Types::STRING, length: 256, nullable: true)]
    private ?string $realName = null;

    #[ORM\Column(type: Types::STRING, length: 256)]
    private string $url;

    #[ORM\Column(name: 'published_content_id', type: Types::INTEGER, nullable: true)]
    private ?string $publishedContentId = null;

    #[ORM\Column(type: Types::STRING, length: 256, nullable: true)]
    private ?string $path = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $content = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private int $status;

    #[ORM\Column(type: Types::STRING, length: 64, nullable: true)]
    private ?string $type = null;

    protected static string $dirName = 'document';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getRealName(): ?string
    {
        return $this->realName;
    }

    public function setRealName(string $realName): self
    {
        $this->realName = $realName;
        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    public function getPublishedContentId(): ?string
    {
        return $this->publishedContentId;
    }

    public function setPublishedContentId(?string $publishedContentId): self
    {
        $this->publishedContentId = $publishedContentId;
        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getDirName(): string
    {
        return DIRECTORY_SEPARATOR . self::$dirName . DIRECTORY_SEPARATOR . (new DateTime())->format('Y_m_d') . DIRECTORY_SEPARATOR;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }
}
