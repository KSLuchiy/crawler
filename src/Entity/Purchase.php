<?php

namespace App\Entity;

use App\Repository\PurchaseRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[
    ORM\Entity(repositoryClass: PurchaseRepository::class),
    ORM\Table(name: 'purchase'),
]
class Purchase
{
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(type: Types::STRING, length: 32)]
    private string $purchaseNumber;

    #[ORM\Column(type: Types::STRING, length: 2000)]
    private string $purchaseName;

    #[ORM\OneToMany(mappedBy: 'purchase', targetEntity: Complaint::class)]
    private Collection $complaints;

    public function __construct()
    {
        $this->complaints = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Purchase
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPurchaseNumber(): string
    {
        return $this->purchaseNumber;
    }

    /**
     * @param string $purchaseNumber
     * @return Purchase
     */
    public function setPurchaseNumber(string $purchaseNumber): self
    {
        $this->purchaseNumber = $purchaseNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getPurchaseName(): string
    {
        return $this->purchaseName;
    }

    /**
     * @param string $purchaseName
     * @return Purchase
     */
    public function setPurchaseName(string $purchaseName): self
    {
        $this->purchaseName = $purchaseName;
        return $this;
    }
}
