<?php

namespace App\Entity\Document;

use App\Entity\ComplaintResult;
use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ComplaintResultDocument extends Document
{
    public const DISCRIMINATOR = 'complaint-result';

    #[ORM\ManyToOne(targetEntity: ComplaintResult::class, cascade: ["persist"], inversedBy: 'documents')]
    #[ORM\JoinColumn(name: 'object_id', referencedColumnName: 'id')]
    private ComplaintResult $complaintResult;

    /**
     * @return ComplaintResult
     */
    public function getComplaintResult(): ComplaintResult
    {
        return $this->complaintResult;
    }

    /**
     * @param ComplaintResult $complaintResult
     * @return ComplaintResultDocument
     */
    public function setComplaintResult(ComplaintResult $complaintResult): self
    {
        $this->complaintResult = $complaintResult;
        return $this;
    }
}
