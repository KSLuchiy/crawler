<?php

namespace App\Entity\Document;

use App\Entity\Complaint;
use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ComplaintDocument extends Document
{
    public const DISCRIMINATOR = 'complaint';

    #[ORM\ManyToOne(targetEntity: Complaint::class, cascade: ["persist"], inversedBy: 'documents')]
    #[ORM\JoinColumn(name: 'object_id', referencedColumnName: 'id')]
    private Complaint $complaint;

    /**
     * @return Complaint
     */
    public function getComplaint(): Complaint
    {
        return $this->complaint;
    }

    /**
     * @param Complaint $complaint
     * @return ComplaintDocument
     */
    public function setComplaint(Complaint $complaint): self
    {
        $this->complaint = $complaint;
        return $this;
    }
}
