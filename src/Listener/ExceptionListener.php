<?php

namespace App\Listener;

use App\Exception\DtoValidationException;
use App\Normalizer\Factory\NormalizerFactory;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Throwable;

class ExceptionListener
{
    /**
     * @var NormalizerFactory
     */
    private NormalizerFactory $normalizerFactory;

    /**
     * @param NormalizerFactory $normalizerFactory
     */
    public function __construct(NormalizerFactory $normalizerFactory)
    {
        $this->normalizerFactory = $normalizerFactory;
    }

    /**
     * @param ExceptionEvent $event
     * @throws ExceptionInterface
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $event->setResponse($this->createApiResponse($exception));
    }

    /**
     * @param Throwable $exception
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    private function createApiResponse(Throwable $exception): JsonResponse
    {
        $normalizer = $this->normalizerFactory->getNormalizer($exception);
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
        }

        $errors = [
            'message' => $exception->getMessage()
        ];

        if ($exception instanceof DtoValidationException) {
            try {
                $errors = [
                    'errors' => $normalizer ? $normalizer->normalize($exception) : []
                ];
            } catch (Exception $e) {
                $errors = [
                    'errors' => []
                ];
            }
        }

        return new JsonResponse(
            $errors,
            $statusCode
        );
    }
}
