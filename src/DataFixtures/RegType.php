<?php

namespace App\DataFixtures;

use App\Entity\RegType as EntityRegType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RegType extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $regTypes = [
            'M' => 'Ручное заведение жалобы',
            'E' => 'Электронная подача',
            'I' => 'Полученная по интеграции',
            'P' => 'Полученная из личного кабинета поставщика (не может быть задано при приеме)'
        ];

        foreach ($regTypes as $key => $name) {
            $manager->persist(
                (new EntityRegType())
                    ->setName($name)
                    ->setCode($key)
            );
        }

        $manager->flush();
    }
}
