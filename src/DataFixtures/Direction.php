<?php

namespace App\DataFixtures;

use App\Entity\Direction as EntityDirection;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Direction extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $directions = [
            '44fz' => '44fz',
            '223fz' => '223fz'
        ];

        foreach ($directions as $key => $name) {
            $manager->persist(
                (new EntityDirection())
                    ->setName($name)
                    ->setCode($key)
            );
        }

        $manager->flush();
    }
}
