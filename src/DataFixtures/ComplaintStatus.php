<?php

namespace App\DataFixtures;

use App\Entity\ComplaintStatus as EntityComplaintStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ComplaintStatus extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $fasTypes = [
            'COMPLAINT_VIOLATIONS' => 'Жалоба признана обоснованной',
            'COMPLAINT_NO_VIOLATIONS' => 'Жалоба признана необоснованной',
            'COMPLAINT_PARTLY_VALID' => 'Жалоба признана обоснованной частично',
            'COMPLAINT_NO_VIOLATIONS_LAW' => 'Жалоба признана необоснованной, и при проведении внеплановой проверки выявлены нарушения законодательства о контрактной системе'
        ];

        foreach ($fasTypes as $key => $name) {
            $manager->persist(
                (new EntityComplaintStatus())
                    ->setName($name)
                    ->setCode($key)
            );
        }

        $manager->flush();
    }
}
