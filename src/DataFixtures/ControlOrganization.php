<?php

namespace App\DataFixtures;

use App\Entity\ControlOrganization as EntityControlOrganization;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ControlOrganization extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $regTypes = array(
            0 =>
                array(
                    'reg_num' => '01671000031',
                    'inn' => '7202081799',
                    'kpp' => '720301001',
                    'name' => 'УФАС по Тюменской обл.',
                ),
            1 =>
                array(
                    'reg_num' => '01731000120',
                    'inn' => '7703516539',
                    'kpp' => '770301001',
                    'name' => 'ФАС России',
                ),
            2 =>
                array(
                    'reg_num' => '01731000090',
                    'inn' => '7706096339',
                    'kpp' => '770101001',
                    'name' => 'УФАС по г.Москве',
                ),
            3 =>
                array(
                    'reg_num' => '03871000152',
                    'inn' => '8601009316',
                    'kpp' => '860101001',
                    'name' => 'УФАС по Ханты-Мансийскому АО-Югре',
                ),
            4 =>
                array(
                    'reg_num' => '01721000054',
                    'inn' => '7825413361',
                    'kpp' => '780101001',
                    'name' => 'УФАС по Санкт-Петербургу',
                ),
            5 =>
                array(
                    'reg_num' => '01301000062',
                    'inn' => '3525048696',
                    'kpp' => '352501001',
                    'name' => 'УФАС по Вологодской обл.',
                ),
            6 =>
                array(
                    'reg_num' => '01531000032',
                    'inn' => '5610042191',
                    'kpp' => '561001001',
                    'name' => 'УФАС по Оренбургской обл.',
                ),
            7 =>
                array(
                    'reg_num' => '01181000005',
                    'inn' => '2309053192',
                    'kpp' => '231201001',
                    'name' => 'УФАС по Краснодарскому краю',
                ),
            8 =>
                array(
                    'reg_num' => '01451000049',
                    'inn' => '7840396953',
                    'kpp' => '784201001',
                    'name' => 'УФАС по Ленинградской обл.',
                ),
            9 =>
                array(
                    'reg_num' => '01241000043',
                    'inn' => '2901061919',
                    'kpp' => '290101001',
                    'name' => 'УФАС по Архангельской обл.',
                ),
            10 =>
                array(
                    'reg_num' => '01482000013',
                    'inn' => '5001034450',
                    'kpp' => '500101001',
                    'name' => 'ГКУ Московской обл.',
                ),
            11 =>
                array(
                    'reg_num' => '01351000046',
                    'inn' => '3905011090',
                    'kpp' => '390601001',
                    'name' => 'УФАС по Калининградской обл.',
                ),
            12 =>
                array(
                    'reg_num' => '01061000030',
                    'inn' => '1001041153',
                    'kpp' => '100101001',
                    'name' => 'УФАС по Респ. Карелия',
                ),
            13 =>
                array(
                    'reg_num' => '01261000077',
                    'inn' => '3123084662',
                    'kpp' => '312301001',
                    'name' => 'УФАС по Белгородской обл.',
                ),
            14 =>
                array(
                    'reg_num' => '01221000063',
                    'inn' => '2721023142',
                    'kpp' => '272101001',
                    'name' => 'УФАС по Хабаровскому краю',
                ),
            15 =>
                array(
                    'reg_num' => '03691000448',
                    'inn' => '7453045147',
                    'kpp' => '745301001',
                    'name' => 'УФАС по Челябинской обл.',
                ),
            16 =>
                array(
                    'reg_num' => '01551000067',
                    'inn' => '5836011815',
                    'kpp' => '583401001',
                    'name' => 'УФАС по Пензенской обл.',
                ),
            17 =>
                array(
                    'reg_num' => '01581000179',
                    'inn' => '6163030500',
                    'kpp' => '616301001',
                    'name' => 'УФАС по Ростовской обл.',
                ),
            18 =>
                array(
                    'reg_num' => '01651000051',
                    'inn' => '7019027633',
                    'kpp' => '701701001',
                    'name' => 'УФАС по Томской обл.',
                ),
            19 =>
                array(
                    'reg_num' => '01191000122',
                    'inn' => '2466009115',
                    'kpp' => '246601001',
                    'name' => 'УФАС по Красноярскому краю',
                ),
            20 =>
                array(
                    'reg_num' => '01561000126',
                    'inn' => '5902290360',
                    'kpp' => '590201001',
                    'name' => 'УФАС по Пермскому краю',
                ),
            21 =>
                array(
                    'reg_num' => '01341000128',
                    'inn' => '3811020966',
                    'kpp' => '380801001',
                    'name' => 'УФАС по Иркутской обл.',
                ),
            22 =>
                array(
                    'reg_num' => '01371000020',
                    'inn' => '4026003620',
                    'kpp' => '402701001',
                    'name' => 'УФАС по Калужской обл.',
                ),
            23 =>
                array(
                    'reg_num' => '01621000158',
                    'inn' => '6658065103',
                    'kpp' => '665801001',
                    'name' => 'УФАС по Свердловской обл.',
                ),
            24 =>
                array(
                    'reg_num' => '01641000084',
                    'inn' => '6831001163',
                    'kpp' => '682901001',
                    'name' => 'УФАС по Тамбовской обл.',
                ),
            25 =>
                array(
                    'reg_num' => '01751000043',
                    'inn' => '9102007869',
                    'kpp' => '910201001',
                    'name' => 'УФАС по Респ. Крым и гор. Севастополю',
                ),
            26 =>
                array(
                    'reg_num' => '01491000038',
                    'inn' => '5191501854',
                    'kpp' => '519001001',
                    'name' => 'УФАС по Мурманской обл.',
                ),
            27 =>
                array(
                    'reg_num' => '01321000031',
                    'inn' => '5260041262',
                    'kpp' => '526001001',
                    'name' => 'УФАС по Нижегородской обл.',
                ),
            28 =>
                array(
                    'reg_num' => '01401000071',
                    'inn' => '4347021540',
                    'kpp' => '434501001',
                    'name' => 'УФАС по Кировской обл.',
                ),
            29 =>
                array(
                    'reg_num' => '01201000072',
                    'inn' => '2540017193',
                    'kpp' => '254001001',
                    'name' => 'УФАС по Приморскому краю',
                ),
            30 =>
                array(
                    'reg_num' => '01031000103',
                    'inn' => '562044239',
                    'kpp' => '57201001',
                    'name' => 'УФАС по Респ. Дагестан',
                ),
            31 =>
                array(
                    'reg_num' => '01021000042',
                    'inn' => '323057082',
                    'kpp' => '32601001',
                    'name' => 'УФАС по Респ. Бурятия',
                ),
            32 =>
                array(
                    'reg_num' => '01274000001',
                    'inn' => '3234034811',
                    'kpp' => '325701001',
                    'name' => 'УФАС по Брянской обл.',
                ),
            33 =>
                array(
                    'reg_num' => '01421000002',
                    'inn' => '6315802344',
                    'kpp' => '631601001',
                    'name' => 'УФАС по Самарской обл.',
                ),
            34 =>
                array(
                    'reg_num' => '01481000016',
                    'inn' => '7703671069',
                    'kpp' => '773401001',
                    'name' => 'УФАС по Московской обл.',
                ),
            35 =>
                array(
                    'reg_num' => '01521000080',
                    'inn' => '5503023028',
                    'kpp' => '550401001',
                    'name' => 'УФАС по Омской обл.',
                ),
            36 =>
                array(
                    'reg_num' => '01011000003',
                    'inn' => '274090077',
                    'kpp' => '27401001',
                    'name' => 'УФАС по Респ. Башкортостан',
                ),
            37 =>
                array(
                    'reg_num' => '01771000009',
                    'inn' => '411073679',
                    'kpp' => '41101001',
                    'name' => 'УФАС по Респ. Алтай',
                ),
            38 =>
                array(
                    'reg_num' => '01311000128',
                    'inn' => '3664022568',
                    'kpp' => '366601001',
                    'name' => 'УФАС по Воронежской обл.',
                ),
            39 =>
                array(
                    'reg_num' => '01161000012',
                    'inn' => '1435137122',
                    'kpp' => '143501001',
                    'name' => 'УФАС по Респ. Саха (Якутия)',
                ),
            40 =>
                array(
                    'reg_num' => '01901000039',
                    'inn' => '8901003347',
                    'kpp' => '890101001',
                    'name' => 'УФАС по Ямало-Ненецкому АО',
                ),
            41 =>
                array(
                    'reg_num' => '01461000067',
                    'inn' => '4826018513',
                    'kpp' => '482601001',
                    'name' => 'УФАС по Липецкой обл.',
                ),
            42 =>
                array(
                    'reg_num' => '01381000054',
                    'inn' => '4101036307',
                    'kpp' => '410101001',
                    'name' => 'УФАС по Камчатскому краю',
                ),
            43 =>
                array(
                    'reg_num' => '01571000050',
                    'inn' => '6027026536',
                    'kpp' => '602701001',
                    'name' => 'УФАС по Псковской обл.',
                ),
            44 =>
                array(
                    'reg_num' => '01661000073',
                    'inn' => '7107026090',
                    'kpp' => '710601001',
                    'name' => 'УФАС по Тульской обл.',
                ),
            45 =>
                array(
                    'reg_num' => '01391000070',
                    'inn' => '4207012419',
                    'kpp' => '420501001',
                    'name' => 'УФАС по Кемеровской обл.',
                ),
            46 =>
                array(
                    'reg_num' => '03251000202',
                    'inn' => '3015011410',
                    'kpp' => '301501001',
                    'name' => 'УФАС по Астраханской обл.',
                ),
            47 =>
                array(
                    'reg_num' => '01111000049',
                    'inn' => '1653003714',
                    'kpp' => '165501001',
                    'name' => 'УФАС по Респ. Татарстан',
                ),
            48 =>
                array(
                    'reg_num' => '03801000056',
                    'inn' => '1901021801',
                    'kpp' => '190101001',
                    'name' => 'УФАС по Респ. Хакасия',
                ),
            49 =>
                array(
                    'reg_num' => '01071000027',
                    'inn' => '1101481197',
                    'kpp' => '110101001',
                    'name' => 'УФАС по Респ. Коми',
                ),
            50 =>
                array(
                    'reg_num' => '01511000186',
                    'inn' => '5405116098',
                    'kpp' => '540501001',
                    'name' => 'УФАС по Новосибирской обл.',
                ),
            51 =>
                array(
                    'reg_num' => '01211000073',
                    'inn' => '2634003887',
                    'kpp' => '263501001',
                    'name' => 'УФАС по Ставропольскому краю',
                ),
            52 =>
                array(
                    'reg_num' => '01101000018',
                    'inn' => '1501004390',
                    'kpp' => '151301001',
                    'name' => 'УФАС по Респ. Северная Осетия-Алания',
                ),
            53 =>
                array(
                    'reg_num' => '01151000040',
                    'inn' => '2128017971',
                    'kpp' => '213001001',
                    'name' => 'УФАС по Чувашской Респ.',
                ),
            54 =>
                array(
                    'reg_num' => '01611000019',
                    'inn' => '6501026378',
                    'kpp' => '650101001',
                    'name' => 'УФАС по Сахалинской обл.',
                ),
            55 =>
                array(
                    'reg_num' => '01501000051',
                    'inn' => '5321047553',
                    'kpp' => '532101001',
                    'name' => 'УФАС по Новгородской обл.',
                ),
            56 =>
                array(
                    'reg_num' => '01591000081',
                    'inn' => '6231010720',
                    'kpp' => '623401001',
                    'name' => 'УФАС по Рязанской обл.',
                ),
            57 =>
                array(
                    'reg_num' => '01291000055',
                    'inn' => '3444051210',
                    'kpp' => '344401001',
                    'name' => 'УФАС по Волгоградской обл.',
                ),
            58 =>
                array(
                    'reg_num' => '01331000071',
                    'inn' => '3728012720',
                    'kpp' => '370201001',
                    'name' => 'УФАС по Ивановской обл.',
                ),
            59 =>
                array(
                    'reg_num' => '03711000089',
                    'inn' => '7604009440',
                    'kpp' => '760401001',
                    'name' => 'УФАС по Ярославской обл.',
                ),
            60 =>
                array(
                    'reg_num' => '01081000013',
                    'inn' => '1215026787',
                    'kpp' => '121501001',
                    'name' => 'УФАС по Респ. Марий Эл',
                ),
            61 =>
                array(
                    'reg_num' => '01781000016',
                    'inn' => '7901532980',
                    'kpp' => '790101001',
                    'name' => 'УФАС по Еврейской АО',
                ),
            62 =>
                array(
                    'reg_num' => '01431000120',
                    'inn' => '4501099573',
                    'kpp' => '450101001',
                    'name' => 'УФАС по Курганской обл.',
                ),
            63 =>
                array(
                    'reg_num' => '01441000119',
                    'inn' => '4629015760',
                    'kpp' => '463201001',
                    'name' => 'УФАС по Курской обл.',
                ),
            64 =>
                array(
                    'reg_num' => '01231000063',
                    'inn' => '2801031325',
                    'kpp' => '280101001',
                    'name' => 'УФАС по Амурской обл.',
                ),
            65 =>
                array(
                    'reg_num' => '01171000215',
                    'inn' => '2221022528',
                    'kpp' => '222501001',
                    'name' => 'УФАС по Алтайскому краю',
                ),
            66 =>
                array(
                    'reg_num' => '01911000063',
                    'inn' => '7536033755',
                    'kpp' => '753601001',
                    'name' => 'УФАС по Забайкальскому краю',
                ),
            67 =>
                array(
                    'reg_num' => '01681000044',
                    'inn' => '7325002331',
                    'kpp' => '732501001',
                    'name' => 'УФАС по Ульяновской обл.',
                ),
            68 =>
                array(
                    'reg_num' => '01541000015',
                    'inn' => '5753018207',
                    'kpp' => '575301001',
                    'name' => 'УФАС по Орловской обл.',
                ),
            69 =>
                array(
                    'reg_num' => '01361000068',
                    'inn' => '6905005800',
                    'kpp' => '695001001',
                    'name' => 'УФАС по Тверской обл.',
                ),
            70 =>
                array(
                    'reg_num' => '01411000083',
                    'inn' => '4401004867',
                    'kpp' => '440101001',
                    'name' => 'УФАС по Костромской обл.',
                ),
            71 =>
                array(
                    'reg_num' => '01041000019',
                    'inn' => '711027218',
                    'kpp' => '72501001',
                    'name' => 'УФАС Кабардино-Балкарской Респ.',
                ),
            72 =>
                array(
                    'reg_num' => '03141000069',
                    'inn' => '608010041',
                    'kpp' => '60801001',
                    'name' => 'УФАС по Респ. Ингушетия',
                ),
            73 =>
                array(
                    'reg_num' => '01281000032',
                    'inn' => '3328101887',
                    'kpp' => '332901001',
                    'name' => 'УФАС по Владимирской обл.',
                ),
            74 =>
                array(
                    'reg_num' => '01131000050',
                    'inn' => '1831038485',
                    'kpp' => '184101001',
                    'name' => 'УФАС по Удмуртской Респ.',
                ),
            75 =>
                array(
                    'reg_num' => '01631000077',
                    'inn' => '6730031796',
                    'kpp' => '673001001',
                    'name' => 'УФАС по Смоленской обл.',
                ),
            76 =>
                array(
                    'reg_num' => '01761000008',
                    'inn' => '105019739',
                    'kpp' => '10501001',
                    'name' => 'УФАС по Респ. Адыгея',
                ),
            77 =>
                array(
                    'reg_num' => '01601000045',
                    'inn' => '6450014580',
                    'kpp' => '645501001',
                    'name' => 'УФАС по Саратовской обл.',
                ),
            78 =>
                array(
                    'reg_num' => '01062000001',
                    'inn' => '1001040590',
                    'kpp' => '100101001',
                    'name' => 'Министерство финансов Респ. Карелия',
                ),
            79 =>
                array(
                    'reg_num' => '01091000075',
                    'inn' => '1326136739',
                    'kpp' => '132601001',
                    'name' => 'УФАС по Респ. Мордовия',
                ),
            80 =>
                array(
                    'reg_num' => '01791000022',
                    'inn' => '901024631',
                    'kpp' => '90101001',
                    'name' => 'УФАС по Карачаево-Черкесской Респ.',
                ),
            81 =>
                array(
                    'reg_num' => '01051000028',
                    'inn' => '816005412',
                    'kpp' => '81601001',
                    'name' => 'УФАС по Респ. Калмыкия',
                ),
            82 =>
                array(
                    'reg_num' => '01471000019',
                    'inn' => '4909053335',
                    'kpp' => '490901001',
                    'name' => 'УФАС по Магаданской обл.',
                ),
            83 =>
                array(
                    'reg_num' => '01182000001',
                    'inn' => '2308120720',
                    'kpp' => '230801001',
                    'name' => 'Министерство экономики Краснодарского края',
                ),
            84 =>
                array(
                    'reg_num' => '01271000094',
                    'inn' => '3234034811',
                    'kpp' => '325701001',
                    'name' => 'УФАС по Брянской обл.',
                ),
            85 =>
                array(
                    'reg_num' => '01841000001',
                    'inn' => '2983007250',
                    'kpp' => '298301001',
                    'name' => 'УФАС по Ненецкому АО',
                ),
            86 =>
                array(
                    'reg_num' => '01121000006',
                    'inn' => '1701044223',
                    'kpp' => '170101001',
                    'name' => 'УФАС по Респ. Тыва',
                ),
            87 =>
                array(
                    'reg_num' => '01881000031',
                    'inn' => '8709012360',
                    'kpp' => '870901001',
                    'name' => 'УФАС по Чукотскому АО',
                ),
            88 =>
                array(
                    'reg_num' => '01941000051',
                    'inn' => '2016002374',
                    'kpp' => '201601001',
                    'name' => 'УФАС по Чеченской Респ.',
                ),
            89 =>
                array(
                    'reg_num' => '01612000003',
                    'inn' => '6500005657',
                    'kpp' => '650101001',
                    'name' => 'Министерство финансов Сахалинской обл.',
                ),
        );

        foreach ($regTypes as $name) {
            $manager->persist(
                (new EntityControlOrganization())
                    ->setInn($name['inn'])
                    ->setKpp($name['kpp'])
                    ->setName($name['name'])
                    ->setRegNum($name['reg_num'])
            );
        }

        $manager->flush();
    }
}
