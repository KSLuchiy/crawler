##Crontab:
Запуск 1 раз в день в 5 утра(zip архивы обновляются в 4 утра)

```sh
0 5 * * * symfony console app:sync-integration-packet --source='/fcs_fas/complaint/currMonth/'
```

##Crontab:
Запуск 1 раз в день в 8 утра

```sh
0 8 * * * symfony console app:sync-integration-packet --source='/fcs_fas/checkResult/currMonth/'
```

##Загрузка всех пакетов старше 2021-01-01
```sh
symfony console app:sync-integration-packet --source='/fcs_fas/complaint/' --min-date='2021-01-01'
```

##Запуск демонов:

Распаковка скаченных ZIP:
```sh
symfony console app:unzip-integration-packet
```

Парсинг XML после загрузки:
```sh
symfony console app:parse-integration-packet
```

Загрузка/Парсинг документов:
```sh
symfony console app:process-complaint-document
```


Заполнение моделей начальными данными:
```sh
symfony console doctrine:fixtures:load
```

Создание elasticsearch:
```sh
symfony console fos:elastica:create
```

Индексация elasticsearch:
```sh
/var/www/rad-crawler/scripts/elastica_populate.sh
symfony console fos:elastica:populate
```

Для работы в php-fpm необходим запущенный unoconv --listener
Права доступа 777 на папки
/var/www/rad-crawler/docker/logs
/var/www/rad-crawler/docker/var
/var/www/rad-crawler/var
