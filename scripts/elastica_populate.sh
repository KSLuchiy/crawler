#!/bin/bash
for i in {1..2000} # Обработает 100000 записей из таблицы checkResult
  do
    /var/www/rad-crawler/bin/console fos:elastica:populate --no-reset --first-page="$i" --last-page="$((i + 1))" --max-per-page=50
  done
/var/www/rad-crawler/bin/console fos:elastica:populate --no-reset --first-page=1 --last-page=2 --max-per-page=50