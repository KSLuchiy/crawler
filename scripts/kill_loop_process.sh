#!/bin/bash
# This script will kill process which running more than X hours
# egrep: the selected process; grep: hours
PIDS="`ps eaxo bsdtime,pid,comm | egrep "pdftohtml|unoconv|soffice.bin" | grep " 1:" | awk '{print $2}'`"

# Kill the process
echo "Killing pdftohtml|unoconv|soffice.bin processes running more than 5 minutes..."
for i in ${PIDS}; do { echo "Killing $i"; kill -9 $i; }; done;