<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211216101600 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("create table organization (id serial not null, name varchar(256) not null, inn varchar(16), kpp varchar(16), reg_num varchar(16), discriminator varchar(255) not null);");
        $this->addSql("create unique index organization_id_uindex on organization (id);");
        $this->addSql("alter table organization add constraint organization_pk primary key (id);");
        $this->addSql("comment on table organization is 'Организации (controlSubjectOrganisation, applicantOrganisation)';");
        $this->addSql("comment on column organization.name is 'Наименование оргинизации';");
        $this->addSql("comment on column organization.inn is 'Инн';");
        $this->addSql("comment on column organization.kpp is 'kpp';");
        $this->addSql("comment on column organization.reg_num is 'Рег. номер';");
        $this->addSql("alter table complaint add applicant_organization_id int;");
        $this->addSql("comment on column complaint.applicant_organization_id is 'ИНН, наименование организации Заявителя';");
        $this->addSql("alter table complaint add constraint complaint_organization_id_fk foreign key (applicant_organization_id) references organization (id);");
        $this->addSql("alter table complaint_result	add control_subject_organization_id int not null;");
        $this->addSql("alter table complaint_result	add constraint complaint_result_organization_id_fk foreign key (control_subject_organization_id) references organization;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
