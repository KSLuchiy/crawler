<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221021161454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'RAD-50040';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE control_organization ADD full_name VARCHAR(2000) DEFAULT NULL');
        $this->addSql('ALTER TABLE control_organization ADD archive BOOLEAN DEFAULT false');

        $this->addSql("UPDATE public.control_organization SET archive = true;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01671000031', cons_registry_num = null, name = 'УФАС по Тюменской обл.', inn = '7202081799', kpp = '720301001', full_name = 'Тюменское УФАС России', archive = false WHERE id = 1;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01731000090', cons_registry_num = null, name = 'УФАС по г.Москве', inn = '7706096339', kpp = '770101001', full_name = 'Московское УФАС России', archive = false WHERE id = 3;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '03871000152', cons_registry_num = null, name = 'УФАС по Ханты-Мансийскому АО-Югре', inn = '8601009316', kpp = '860101001', full_name = 'Ханты-Мансийское УФАС России', archive = false WHERE id = 4;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01721000054', cons_registry_num = null, name = 'УФАС по Санкт-Петербургу', inn = '7825413361', kpp = '780101001', full_name = 'Санкт-Петербургское УФАС России', archive = false WHERE id = 5;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01301000062', cons_registry_num = null, name = 'УФАС по Вологодской обл.', inn = '3525048696', kpp = '352501001', full_name = 'Вологодское УФАС России', archive = false WHERE id = 6;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01531000032', cons_registry_num = null, name = 'УФАС по Оренбургской обл.', inn = '5610042191', kpp = '561001001', full_name = 'Оренбургское УФАС России', archive = false WHERE id = 7;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01181000005', cons_registry_num = null, name = 'УФАС по Краснодарскому краю', inn = '2309053192', kpp = '231201001', full_name = 'Краснодарское УФАС России', archive = false WHERE id = 8;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01451000049', cons_registry_num = null, name = 'УФАС по Ленинградской обл.', inn = '7840396953', kpp = '784201001', full_name = 'Ленинградское УФАС России', archive = false WHERE id = 9;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01241000043', cons_registry_num = null, name = 'УФАС по Архангельской обл.', inn = '2901061919', kpp = '290101001', full_name = 'Архангельское УФАС России', archive = false WHERE id = 10;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01351000046', cons_registry_num = null, name = 'УФАС по Калининградской обл.', inn = '3905011090', kpp = '390601001', full_name = 'Калининградское УФАС России', archive = false WHERE id = 12;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01061000030', cons_registry_num = null, name = 'УФАС по Респ. Карелия', inn = '1001041153', kpp = '100101001', full_name = 'Карельское УФАС России', archive = false WHERE id = 13;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01261000077', cons_registry_num = null, name = 'УФАС по Белгородской обл.', inn = '3123084662', kpp = '312301001', full_name = 'Белгородское УФАС России', archive = false WHERE id = 14;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01221000063', cons_registry_num = null, name = 'УФАС по Хабаровскому краю', inn = '2721023142', kpp = '272101001', full_name = 'Хабаровское УФАС России', archive = false WHERE id = 15;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '03691000448', cons_registry_num = null, name = 'УФАС по Челябинской обл.', inn = '7453045147', kpp = '745301001', full_name = 'Челябинское УФАС России', archive = false WHERE id = 16;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01551000067', cons_registry_num = null, name = 'УФАС по Пензенской обл.', inn = '5836011815', kpp = '583401001', full_name = 'Пензенское УФАС России', archive = false WHERE id = 17;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01581000179', cons_registry_num = null, name = 'УФАС по Ростовской обл.', inn = '6163030500', kpp = '616301001', full_name = 'Ростовское УФАС России', archive = false WHERE id = 18;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01651000051', cons_registry_num = null, name = 'УФАС по Томской обл.', inn = '7019027633', kpp = '701701001', full_name = 'Томское УФАС России', archive = false WHERE id = 19;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01191000122', cons_registry_num = null, name = 'УФАС по Красноярскому краю', inn = '2466009115', kpp = '246601001', full_name = 'Красноярское УФАС России', archive = false WHERE id = 20;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01561000126', cons_registry_num = null, name = 'УФАС по Пермскому краю', inn = '5902290360', kpp = '590201001', full_name = 'Пермское УФАС России', archive = false WHERE id = 21;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01341000128', cons_registry_num = null, name = 'УФАС по Иркутской обл.', inn = '3811020966', kpp = '380801001', full_name = 'Иркутское УФАС России', archive = false WHERE id = 22;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01371000020', cons_registry_num = null, name = 'УФАС по Калужской обл.', inn = '4026003620', kpp = '402701001', full_name = 'Калужское УФАС России', archive = false WHERE id = 23;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01621000158', cons_registry_num = null, name = 'УФАС по Свердловской обл.', inn = '6658065103', kpp = '665801001', full_name = 'Свердловское УФАС России', archive = false WHERE id = 24;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01641000084', cons_registry_num = null, name = 'УФАС по Тамбовской обл.', inn = '6831001163', kpp = '682901001', full_name = 'Тамбовское УФАС России', archive = false WHERE id = 25;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01751000043', cons_registry_num = null, name = 'УФАС по Респ. Крым и гор. Севастополю', inn = '9102007869', kpp = '910201001', full_name = 'Крымское УФАС России', archive = false WHERE id = 26;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01491000038', cons_registry_num = null, name = 'УФАС по Мурманской обл.', inn = '5191501854', kpp = '519001001', full_name = 'Мурманское УФАС России', archive = false WHERE id = 27;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01321000031', cons_registry_num = null, name = 'УФАС по Нижегородской обл.', inn = '5260041262', kpp = '526001001', full_name = 'Нижегородское УФАС России', archive = false WHERE id = 28;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01401000071', cons_registry_num = null, name = 'УФАС по Кировской обл.', inn = '4347021540', kpp = '434501001', full_name = 'Кировское УФАС России', archive = false WHERE id = 29;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01201000072', cons_registry_num = null, name = 'УФАС по Приморскому краю', inn = '2540017193', kpp = '254001001', full_name = 'Приморское УФАС России', archive = false WHERE id = 30;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01031000103', cons_registry_num = null, name = 'УФАС по Респ. Дагестан', inn = '562044239', kpp = '57201001', full_name = 'Дагестанское УФАС России', archive = false WHERE id = 31;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01021000042', cons_registry_num = null, name = 'УФАС по Респ. Бурятия', inn = '323057082', kpp = '32601001', full_name = 'Бурятское УФАС России', archive = false WHERE id = 32;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01421000002', cons_registry_num = null, name = 'УФАС по Самарской обл.', inn = '6315802344', kpp = '631601001', full_name = 'Самарское УФАС России', archive = false WHERE id = 34;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01481000016', cons_registry_num = null, name = 'УФАС по Московской обл.', inn = '7703671069', kpp = '773401001', full_name = 'Московское областное УФАС России', archive = false WHERE id = 35;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01521000080', cons_registry_num = null, name = 'УФАС по Омской обл.', inn = '5503023028', kpp = '550401001', full_name = 'Омское УФАС России', archive = false WHERE id = 36;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01011000003', cons_registry_num = null, name = 'УФАС по Респ. Башкортостан', inn = '274090077', kpp = '27401001', full_name = 'Башкортостанское УФАС России', archive = false WHERE id = 37;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01771000009', cons_registry_num = null, name = 'УФАС по Респ. Алтай', inn = '411073679', kpp = '41101001', full_name = 'Алтайское республиканское УФАС России', archive = false WHERE id = 38;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01311000128', cons_registry_num = null, name = 'УФАС по Воронежской обл.', inn = '3664022568', kpp = '366601001', full_name = 'Воронежское УФАС России', archive = false WHERE id = 39;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01161000012', cons_registry_num = null, name = 'УФАС по Респ. Саха (Якутия)', inn = '1435137122', kpp = '143501001', full_name = 'Якутское УФАС России', archive = false WHERE id = 40;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01901000039', cons_registry_num = null, name = 'УФАС по Ямало-Ненецкому АО', inn = '8901003347', kpp = '890101001', full_name = 'Ямало-Ненецкое УФАС России', archive = false WHERE id = 41;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01461000067', cons_registry_num = null, name = 'УФАС по Липецкой обл.', inn = '4826018513', kpp = '482601001', full_name = 'Липецкое УФАС России', archive = false WHERE id = 42;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01381000054', cons_registry_num = null, name = 'УФАС по Камчатскому краю', inn = '4101036307', kpp = '410101001', full_name = 'Камчатское УФАС России', archive = false WHERE id = 43;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01571000050', cons_registry_num = null, name = 'УФАС по Псковской обл.', inn = '6027026536', kpp = '602701001', full_name = 'Псковское УФАС России', archive = false WHERE id = 44;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01661000073', cons_registry_num = null, name = 'УФАС по Тульской обл.', inn = '7107026090', kpp = '710601001', full_name = 'Тульское УФАС России', archive = false WHERE id = 45;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01391000070', cons_registry_num = null, name = 'УФАС по Кемеровской обл.', inn = '4207012419', kpp = '420501001', full_name = 'Кемеровское УФАС России', archive = false WHERE id = 46;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '03251000202', cons_registry_num = null, name = 'УФАС по Астраханской обл.', inn = '3015011410', kpp = '301501001', full_name = 'Астраханское УФАС России', archive = false WHERE id = 47;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01111000049', cons_registry_num = null, name = 'УФАС по Респ. Татарстан', inn = '1653003714', kpp = '165501001', full_name = 'Татарстанское УФАС России', archive = false WHERE id = 48;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '03801000056', cons_registry_num = null, name = 'УФАС по Респ. Хакасия', inn = '1901021801', kpp = '190101001', full_name = 'Хакасское УФАС России', archive = false WHERE id = 49;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01071000027', cons_registry_num = null, name = 'УФАС по Респ. Коми', inn = '1101481197', kpp = '110101001', full_name = 'Коми УФАС России', archive = false WHERE id = 50;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01511000186', cons_registry_num = null, name = 'УФАС по Новосибирской обл.', inn = '5405116098', kpp = '540501001', full_name = 'Новосибирское УФАС России', archive = false WHERE id = 51;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01211000073', cons_registry_num = null, name = 'УФАС по Ставропольскому краю', inn = '2634003887', kpp = '263501001', full_name = 'Ставропольское УФАС России', archive = false WHERE id = 52;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01101000018', cons_registry_num = null, name = 'УФАС по Респ. Северная Осетия-Алания', inn = '1501004390', kpp = '151301001', full_name = 'Северо-Осетинское УФАС России', archive = false WHERE id = 53;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01151000040', cons_registry_num = null, name = 'УФАС по Чувашской Респ.', inn = '2128017971', kpp = '213001001', full_name = 'Чувашское УФАС России', archive = false WHERE id = 54;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01611000019', cons_registry_num = null, name = 'УФАС по Сахалинской обл.', inn = '6501026378', kpp = '650101001', full_name = 'Сахалинское УФАС России', archive = false WHERE id = 55;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01501000051', cons_registry_num = null, name = 'УФАС по Новгородской обл.', inn = '5321047553', kpp = '532101001', full_name = 'Новгородское УФАС России', archive = false WHERE id = 56;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01591000081', cons_registry_num = null, name = 'УФАС по Рязанской обл.', inn = '6231010720', kpp = '623401001', full_name = 'Рязанское УФАС России', archive = false WHERE id = 57;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01291000055', cons_registry_num = null, name = 'УФАС по Волгоградской обл.', inn = '3444051210', kpp = '344401001', full_name = 'Волгоградское УФАС России', archive = false WHERE id = 58;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01331000071', cons_registry_num = null, name = 'УФАС по Ивановской обл.', inn = '3728012720', kpp = '370201001', full_name = 'Ивановское УФАС России', archive = false WHERE id = 59;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '03711000089', cons_registry_num = null, name = 'УФАС по Ярославской обл.', inn = '7604009440', kpp = '760401001', full_name = 'Ярославское УФАС России', archive = false WHERE id = 60;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01081000013', cons_registry_num = null, name = 'УФАС по Респ. Марий Эл', inn = '1215026787', kpp = '121501001', full_name = 'Марийское УФАС России', archive = false WHERE id = 61;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01781000016', cons_registry_num = null, name = 'УФАС по Еврейской АО', inn = '7901532980', kpp = '790101001', full_name = 'Еврейское УФАС России', archive = false WHERE id = 62;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01431000120', cons_registry_num = null, name = 'УФАС по Курганской обл.', inn = '4501099573', kpp = '450101001', full_name = 'Курганское УФАС России', archive = false WHERE id = 63;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01441000119', cons_registry_num = null, name = 'УФАС по Курской обл.', inn = '4629015760', kpp = '463201001', full_name = 'Курское УФАС России', archive = false WHERE id = 64;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01231000063', cons_registry_num = null, name = 'УФАС по Амурской обл.', inn = '2801031325', kpp = '280101001', full_name = 'Амурское УФАС России', archive = false WHERE id = 65;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01171000215', cons_registry_num = null, name = 'УФАС по Алтайскому краю', inn = '2221022528', kpp = '222501001', full_name = 'Алтайское краевое УФАС России', archive = false WHERE id = 66;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01911000063', cons_registry_num = null, name = 'УФАС по Забайкальскому краю', inn = '7536033755', kpp = '753601001', full_name = 'Забайкальское УФАС России', archive = false WHERE id = 67;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01681000044', cons_registry_num = null, name = 'УФАС по Ульяновской обл.', inn = '7325002331', kpp = '732501001', full_name = 'Ульяновское УФАС России', archive = false WHERE id = 68;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01541000015', cons_registry_num = null, name = 'УФАС по Орловской обл.', inn = '5753018207', kpp = '575301001', full_name = 'Орловское УФАС России', archive = false WHERE id = 69;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01361000068', cons_registry_num = null, name = 'УФАС по Тверской обл.', inn = '6905005800', kpp = '695001001', full_name = 'Тверское УФАС России', archive = false WHERE id = 70;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01411000083', cons_registry_num = null, name = 'УФАС по Костромской обл.', inn = '4401004867', kpp = '440101001', full_name = 'Костромское УФАС России', archive = false WHERE id = 71;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01041000019', cons_registry_num = null, name = 'УФАС Кабардино-Балкарской Респ.', inn = '711027218', kpp = '72501001', full_name = 'Кабардино-Балкарское УФАС России', archive = false WHERE id = 72;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '03141000069', cons_registry_num = null, name = 'УФАС по Респ. Ингушетия', inn = '608010041', kpp = '60801001', full_name = 'Ингушское УФАС России', archive = false WHERE id = 73;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01281000032', cons_registry_num = null, name = 'УФАС по Владимирской обл.', inn = '3328101887', kpp = '332901001', full_name = 'Владимирское УФАС России', archive = false WHERE id = 74;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01131000050', cons_registry_num = null, name = 'УФАС по Удмуртской Респ.', inn = '1831038485', kpp = '184101001', full_name = 'Удмуртское УФАС России', archive = false WHERE id = 75;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01631000077', cons_registry_num = null, name = 'УФАС по Смоленской обл.', inn = '6730031796', kpp = '673001001', full_name = 'Смоленское УФАС России', archive = false WHERE id = 76;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01761000008', cons_registry_num = null, name = 'УФАС по Респ. Адыгея', inn = '105019739', kpp = '10501001', full_name = 'Адыгейское УФАС России', archive = false WHERE id = 77;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01601000045', cons_registry_num = null, name = 'УФАС по Саратовской обл.', inn = '6450014580', kpp = '645501001', full_name = 'Саратовское УФАС России', archive = false WHERE id = 78;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01091000075', cons_registry_num = null, name = 'УФАС по Респ. Мордовия', inn = '1326136739', kpp = '132601001', full_name = 'Мордовское УФАС России', archive = false WHERE id = 80;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01791000022', cons_registry_num = null, name = 'УФАС по Карачаево-Черкесской Респ.', inn = '901024631', kpp = '90101001', full_name = 'Карачаево-Черкесское УФАС России', archive = false WHERE id = 81;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01051000028', cons_registry_num = null, name = 'УФАС по Респ. Калмыкия', inn = '816005412', kpp = '81601001', full_name = 'Калмыцкое УФАС России', archive = false WHERE id = 82;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01471000019', cons_registry_num = null, name = 'УФАС по Магаданской обл.', inn = '4909053335', kpp = '490901001', full_name = 'Магаданское УФАС России', archive = false WHERE id = 83;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01271000094', cons_registry_num = null, name = 'УФАС по Брянской обл.', inn = '3234034811', kpp = '325701001', full_name = 'Брянское УФАС России', archive = false WHERE id = 85;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01841000001', cons_registry_num = null, name = 'УФАС по Ненецкому АО', inn = '2983007250', kpp = '298301001', full_name = 'Ненецкое УФАС России', archive = false WHERE id = 86;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01121000006', cons_registry_num = null, name = 'УФАС по Респ. Тыва', inn = '1701044223', kpp = '170101001', full_name = 'Тывинское УФАС России', archive = false WHERE id = 87;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01881000031', cons_registry_num = null, name = 'УФАС по Чукотскому АО', inn = '8709012360', kpp = '870901001', full_name = 'Чукотское УФАС России', archive = false WHERE id = 88;");
        $this->addSql("UPDATE public.control_organization SET reg_num = '01941000051', cons_registry_num = null, name = 'УФАС по Чеченской Респ.', inn = '2016002374', kpp = '201601001', full_name = 'Чеченское УФАС России', archive = false WHERE id = 89;");
        $this->addSql("UPDATE public.control_organization SET full_name = 'ЦА ФАС России', archive = false WHERE id = 2;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE control_organization DROP full_name');
        $this->addSql('ALTER TABLE control_organization DROP archive');
    }
}
