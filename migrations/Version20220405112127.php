<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220405112127 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE complaint ADD complaint_number VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE complaint ALTER reg_number DROP NOT NULL');
        $this->addSql('ALTER TABLE document DROP doc_number');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE complaint DROP complaint_number');
        $this->addSql('ALTER TABLE complaint ALTER reg_number SET NOT NULL');
        $this->addSql('ALTER TABLE document ADD doc_number VARCHAR(32) NOT NULL');}
}
