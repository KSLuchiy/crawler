<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211209162400 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("comment on column complaint.reg_number is 'Рег.номер процедуры';");
        $this->addSql("comment on column complaint.purchase_id is 'Внутренний id процедуры. Связь с purchase'");
        $this->addSql("comment on column complaint.control_organization_id is 'Внутренний id контр. органа. Связь с control_organization'");
        $this->addSql("comment on column complaint.direction_id is 'Тип закупки. 44, 223'");
        $this->addSql("comment on column complaint.external_id is 'Внешний id жалобы. Выводится в ссылке жалобы. complaint_link'");
        $this->addSql("comment on table complaint is 'Жалобы из пакета complaint'");
        $this->addSql("comment on column complaint_result.complaint_id is 'Внутренний id жалобы. Связь с complaint'");
        $this->addSql("comment on column complaint_result.reasoned_at is 'Дата принятия решения по жалобе'");
        $this->addSql("comment on column complaint_result.status_id is 'Внутренний id решения по жалобе. Связь с complaint_status'");
        $this->addSql("comment on table complaint_result is 'Результат по жалобе'");
        $this->addSql("comment on column complaint_status.code is 'Код жалобы'");
        $this->addSql("comment on table complaint_status is 'Статусы жалоб'");
        $this->addSql("comment on column control_organization.reg_num is 'Рег. номер контр. органа'");
        $this->addSql("comment on column control_organization.name is 'Наименование контр. органа'");
        $this->addSql("comment on table control_organization is 'Контролирующие органы'");
        $this->addSql("comment on column direction.code is 'Код закупки 44fz, 223fz'");
        $this->addSql("comment on table direction is 'Типы закупок'");
        $this->addSql("comment on column document.object_id is 'Внутренний id жалобы или результата жалобы'");
        $this->addSql("comment on column document.discriminator is 'Дискриминатор жалобы complaint или checkResult'");
        $this->addSql("comment on column document.name is 'Имя файла'");
        $this->addSql("comment on column document.real_name is 'Полное имя файла'");
        $this->addSql("comment on column document.url is 'Ссылка на файл'");
        $this->addSql("comment on column document.doc_number is 'Номер документа'");
        $this->addSql("comment on column document.path is 'Внутренний uri'");
        $this->addSql("comment on column document.content is 'Контент файла'");
        $this->addSql("comment on column document.status is 'Статус обработки файла'");
        $this->addSql("comment on column document.type is 'Тип файла prescription..'");
        $this->addSql("comment on table document is 'Приложенные документы по жалобам'");
        $this->addSql("comment on column purchase.purchase_number is 'Рег. номер закупки'");
        $this->addSql("comment on column purchase.purchase_name is 'Название закупки'");
        $this->addSql("comment on table purchase is 'Закупки'");
        $this->addSql("comment on column reg_type.name is 'Название жалобы'");
        $this->addSql("comment on column reg_type.code is 'Код жалобы'");
        $this->addSql("comment on table reg_type is 'Типы жалоб'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
