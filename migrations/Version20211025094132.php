<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211025094132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE integration_packet_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE integration_packet (id INT NOT NULL, parent_id INT DEFAULT NULL, guid VARCHAR(36) NOT NULL, name VARCHAR(50) NOT NULL, path VARCHAR(150) NOT NULL, size INT NOT NULL, modify_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, hash VARCHAR(100) NOT NULL, status SMALLINT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CD1F1661727ACA70 ON integration_packet (parent_id)');
        $this->addSql('ALTER TABLE integration_packet ADD CONSTRAINT FK_CD1F1661727ACA70 FOREIGN KEY (parent_id) REFERENCES integration_packet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE integration_packet DROP CONSTRAINT FK_CD1F1661727ACA70');
        $this->addSql('DROP SEQUENCE integration_packet_id_seq CASCADE');
        $this->addSql('DROP TABLE integration_packet');
    }
}
