<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211109123403 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('create table complaint_status(id serial not null constraint complaint_status_pk primary key, name varchar(256) not null, code varchar(64) not null);');
        $this->addSql('create table reg_type(id serial not null constraint reg_type_pk primary key, name varchar(256) not null, code varchar(64) not null);');
        $this->addSql('create table purchase(id serial not null constraint purchase_pk primary key, purchase_number varchar(32) not null, purchase_name varchar(2000) not null);');
        $this->addSql('create table direction(id serial not null constraint direction_pk primary key, name varchar(64) not null, code varchar(64) not null);');
        $this->addSql('create table complaint(id serial not null constraint complaint_pk primary key, created_at timestamp not null, updated_at timestamp not null, reg_number varchar(32) not null, purchase_id integer not null, control_organization_id integer not null, direction_id integer not null, external_id integer not null);');
        $this->addSql('create table complaint_result(id serial not null constraint complaintresult_pk primary key, complaint_id integer not null, created_at timestamp not null, reasoned_at timestamp not null, status_id integer not null);');
        $this->addSql('create table control_organization(id serial not null constraint control_organization_pk primary key, reg_num varchar(16) not null, cons_registry_num varchar(16), name varchar(2000) not null, inn varchar(16), kpp varchar(16));');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE complaint_status');
        $this->addSql('DROP TABLE reg_type');
        $this->addSql('DROP TABLE purchase');
        $this->addSql('DROP TABLE direction');
        $this->addSql('DROP TABLE complaint');
        $this->addSql('DROP TABLE complaint_result');
        $this->addSql('DROP TABLE control_organization');
    }
}
