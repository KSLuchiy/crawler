<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211109163142 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE document_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('create table document(id integer not null constraint document_pkey primary key, object_id integer not null, discriminator varchar(255) not null, name varchar(256) not null, real_name varchar(256) default NULL::character varying, url varchar(256) not null, doc_number varchar(32) not null, published_content_id integer, path varchar(256) default NULL::character varying, content text, status smallint not null, type varchar(64));');
        $this->addSql('ALTER TABLE integration_packet ALTER hash DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE document_id_seq CASCADE');
        $this->addSql('DROP TABLE document');
        $this->addSql('ALTER TABLE integration_packet ALTER hash SET NOT NULL');
    }
}
