<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211213101600 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("alter table complaint add reg_type_id int not null;");
        $this->addSql("comment on column complaint.reg_type_id is 'Тип подачи жалобы ID';");
        $this->addSql("alter table complaint add version int not null;");
        $this->addSql("comment on column complaint.version is 'Версия пакета';");
        $this->addSql("comment on column complaint.created_at is 'Дата создания. Берется из printForm create_date';");
        $this->addSql("comment on column complaint.updated_at is 'Дата изменения. Берется из printForm edit_date';");
        $this->addSql("comment on column complaint_result.created_at is 'Дата создания. Берется из commonInfo';");
        $this->addSql("comment on column control_organization.cons_registry_num is 'Код организации по Сводному реестру';");
        $this->addSql("alter table complaint add constraint complaint_purchase_id_fk foreign key (purchase_id) references purchase;");
        $this->addSql("alter table complaint add constraint complaint_control_organization_id_fk foreign key (control_organization_id) references control_organization;");
        $this->addSql("alter table complaint add constraint complaint_direction_id_fk foreign key (direction_id) references direction;");
        $this->addSql("alter table complaint add constraint complaint_reg_type_id_fk foreign key (reg_type_id) references reg_type;");
        $this->addSql("alter table complaint_result add constraint complaint_result_complaint_id_fk foreign key (complaint_id) references complaint;");
        $this->addSql("alter table complaint_result	add constraint complaint_result_complaint_status_id_fk foreign key (status_id) references complaint_status;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
