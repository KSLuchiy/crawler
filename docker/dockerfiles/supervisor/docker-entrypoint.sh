#!/bin/bash
set -e

echo "date.timezone='Europe/Moscow'" > /usr/local/etc/php/conf.d/timezone.ini
echo "memory_limit=1024M" > /usr/local/etc/php/php.ini

printenv >> /etc/environment

chmod +x /var/www/rad-crawler/scripts/kill_loop_process.sh

unoconv --listener && unoconv --listener > /var/log/unoconv.log &

touch /var/log/cron.log
crontab /etc/cron.d/sync.conf

cron

exec "$@"